#pragma once

#define NEOS_CONFIG_FILE "nEOS.ini"
#define NEOS_LOG_FILE "nEOS.log"

#define NEOS_HANDLE		0xBEEFCAF0
//#define EOS_HPLATFORM	NEOS_HANDLE+0 // no longer needed
#define NEOS_HMETRICS	NEOS_HANDLE+1
//#define EOS_HAUTH		NEOS_HANDLE+2 // no longer needed, now have HAuth class
#define NEOS_HCONNECT   NEOS_HANDLE+3
#define NEOS_HECOM		NEOS_HANDLE+4
#define NEOS_HFRIENDS	NEOS_HANDLE+5
#define NEOS_HPRESENCE	NEOS_HANDLE+6
#define NEOS_HSESSIONS  NEOS_HANDLE+7
#define NEOS_HUSERINFO	NEOS_HANDLE+8
#define NEOS_HP2P       NEOS_HANDLE+9
#define NEOS_HPLAYERDATASTORAGE NEOS_HANDLE+10
#define NEOS_HACHIEVEMENTS NEOS_HANDLE+11
#define NEOS_HSTATS NEOS_HANDLE+12

#define NEOS_NOTIFY_FIRSTID  (EOS_NotificationId)1234 // 0 is invalid, so choose an index to start at

#define NEOS_NOTIFY2_FIRSTID (EOS_NotificationId)91234 // 0 is invalid, so choose an index to start at

// eos_init.cpp
extern EOS_AllocateMemoryFunc GameAllocMemoryFunc;

// neos_debug.cpp
// LL - LogLevels
enum class LL
{
	Debug = 0,
	Info = 1,
	Warning = 2,
	Error = 3,
	NumLevels = 4,
};

extern bool LogEnabled;
extern std::string LogFilename;
extern int LogLevel;
void log(LL severity, const char* format, ...); 
extern bool NEOS_IsProxyLogged;
void NEOS_InitProxyLogging();

// neos_main.cpp
extern bool EOSIsInited;
extern bool EOSIsShutdown;

inline bool EOS_IsConfigured()
{
	return EOSIsInited && !EOSIsShutdown;
}

// Runtime stuff:
extern HMODULE ProxyLibrary;
extern HMODULE NEOSLibrary;
extern std::filesystem::path NEOSFolder;

extern std::string GameProductName;
extern std::string GameProductVersion;
extern std::string GameProductId;
extern std::string GameSandboxId;
extern std::string GameClientId;
extern std::string GameClientSecret;

// INI settings:
extern bool UseAsyncCallbacks;
extern bool ProxyHookLogs;

extern std::string UserName;
extern EOS_EpicAccountId UserAccountId;
extern EOS_ProductUserId UserProductId;
extern std::string UserLanguage;
extern std::string UserCountry;

extern std::string IniGameProductId;

extern std::string OverrideProductName;
extern std::string OverrideProductVersion;
extern std::string OverrideProductId;
extern std::string OverrideSandboxId;
extern std::string OverrideClientId;
extern std::string OverrideClientSecret;
extern std::string OverrideEncryptionKey;
extern std::string OverrideDeploymentId;

extern bool DLCLogQueries;
extern bool DLCAllOwned;
extern fifo_map<std::string, std::string> DLCOwned;
extern fifo_map<std::string, std::string> DLCForced;
extern std::vector<std::pair<std::string, std::string>> Entitlements;

extern bool ForcedDLCUseMalloc;

bool NEOS_RestartAppIfNecessary();
void NEOS_AddCallback(std::function<void()> fn);
void NEOS_RunCallbacks();
void NEOS_Main();

// neos_util.cpp
LPCWSTR GetCommandLineNoExe();
std::filesystem::path GetDllParentDir(HMODULE hmod);
std::wstring WidenString(const std::string& in);
std::string ToString(uint64_t value);
bool StringToBool(const std::string& str);
bool FileExists(const char* path);

// neos_enumutil.cpp
enum class EOSEnum
{
	EResult,
	ELoginCredentialType,
	EAuthTokenType,
	ELoginStatus,
	EExternalCredentialType,
	EExternalAccountType,
	EOwnershipStatus,
	EEcomItemType,
	EFriendsStatus,
	ELogLevel,
	ELogCategory,
	EUserControllerType,
	EMetricsAccountIdType,
	ENATType,
	EConnectionClosedReason,
	Presence_EStatus,
	EOnlineSessionState,
	ESessionAttributeAdvertisementType,
	ESessionAttributeType,
	EOnlineComparisonOp,
	EOnlineSessionPermissionLevel,

	PlayerDataStorage_EReadResult,
	PlayerDataStorage_EWriteResult,

	NumEnums
};
const char* EnumToString(EOSEnum EnumType, int32_t Value);
const char* EResultToString(EOS_EResult Result);

#include "neos_classes.hpp"
