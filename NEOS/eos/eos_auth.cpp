#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * Release the memory associated with an EOS_Auth_Token structure. This must be called on data retrieved from EOS_Auth_CopyUserAuthToken.
 *
 * @param AuthToken - The auth token structure to be released
 *
 * @see EOS_Auth_Token
 * @see EOS_Auth_CopyUserAuthToken
 */
EOS_DECLARE_FUNC(void) EOS_Auth_Token_Release(EOS_Auth_Token* AuthToken)
{
	log(LL::Debug, "EOS_Auth_Token_Release (%p)", AuthToken);

	PROXY_FUNC(EOS_Auth_Token_Release);
	if (proxied)
	{
		proxied(AuthToken);
		return;
	}

	delete AuthToken;
}

/**
 * The Auth Interface is used to manage local user permissions and access to backend services through the verification of various forms of credentials.
 * All Auth Interface calls take a handle of type EOS_HAuth as the first parameter.
 * This handle can be retrieved from a EOS_HPlatform handle by using the EOS_Platform_GetAuthInterface function.
 *
 * @see EOS_Platform_GetAuthInterface
 */

 /**
  * Login/Authenticate with user credentials.
  *
  * @param Options structure containing the account credentials to use during the login operation
  * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
  * @param CompletionDelegate a callback that is fired when the login operation completes, either successfully or in error
  */
EOS_DECLARE_FUNC(void) EOS_Auth_Login(EOS_HAuth Handle, const EOS_Auth_LoginOptions* Options, void* ClientData, const EOS_Auth_OnLoginCallback CompletionDelegate)
{
	log(LL::Info, "EOS_Auth_Login (%p, %p, %p, %p)", Handle, Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_AUTH_LOGIN_API_LATEST);

	// TODO: code below seems to cause crashes for some ppl, not sure what makes it happen though...
	/*if (Options && Options->Credentials)
	{
		if (Options->Credentials->Id && strlen(Options->Credentials->Id))
			log(LL::Info, " - Login Id: %s", Options->Credentials->Id);
		if (Options->Credentials->Token && strlen(Options->Credentials->Token))
			log(LL::Info, " - Login Token: %s", Options->Credentials->Token);
		log(LL::Info, " - Login Type: %d", Options->Credentials->Type);
	}*/

	PROXY_FUNC(EOS_Auth_Login);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return;
	}

	//currently unused:
	//EOS_Auth_LoginOptions options;
	//if (Options)
	//	options = *Options;

	NEOS_AddCallback([Handle, Options, ClientData, CompletionDelegate]()
		{
			auto* auth = reinterpret_cast<HAuth*>(Handle);

			if (auth->GetLoginStatus() == EOS_ELoginStatus::EOS_LS_LoggedIn)
				log(LL::Warning, __FUNCTION__ ": already logged in!");

			auth->ChangeLoginStatus(EOS_ELoginStatus::EOS_LS_LoggedIn);

			EOS_Auth_LoginCallbackInfo cbi;
			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = UserAccountId;
			cbi.PinGrantInfo = nullptr;
			CompletionDelegate(&cbi);
		});
}

/**
 * Signs the player out of the online service.
 *
 * @param Options structure containing information about which account to log out.
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the logout operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Auth_Logout(EOS_HAuth Handle, const EOS_Auth_LogoutOptions* Options, void* ClientData, const EOS_Auth_OnLogoutCallback CompletionDelegate)
{
	if(Options)
		log(LL::Debug, "EOS_Auth_Logout (%p, %p(%p), %p, %p)", Handle, Options, Options->LocalUserId, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Auth_Logout (%p, %p, %p, %p)", Handle, Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_AUTH_LOGOUT_API_LATEST);

	PROXY_FUNC(EOS_Auth_Logout);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return;
	}

	//currently unused:
	//EOS_Auth_LogoutOptions options;
	//if (Options)
	//	options = *Options;

	NEOS_AddCallback([Handle, Options, ClientData, CompletionDelegate]()
		{
			auto* auth = reinterpret_cast<HAuth*>(Handle);

			if (auth->GetLoginStatus() == EOS_ELoginStatus::EOS_LS_NotLoggedIn)
				log(LL::Warning, __FUNCTION__ ": already logged out!");

			auth->ChangeLoginStatus(EOS_ELoginStatus::EOS_LS_NotLoggedIn);

			EOS_Auth_LogoutCallbackInfo cbi;
			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = UserAccountId;
			CompletionDelegate(&cbi);
		});
}

/**
 * Contact the backend service to verify validity of an existing user auth token.
 * This function is intended for server-side use only.
 *
 * @param Options structure containing information about the auth token being verified
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the logout operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Auth_VerifyUserAuth(EOS_HAuth Handle, const EOS_Auth_VerifyUserAuthOptions* Options, void* ClientData, const EOS_Auth_OnVerifyUserAuthCallback CompletionDelegate)
{
	log(LL::Debug, "EOS_Auth_VerifyUserAuth (%p, %p, %p)", Handle, Options, ClientData);

	EOS_CHECK_VERSION(EOS_AUTH_VERIFYUSERAUTH_API_LATEST);

	PROXY_FUNC(EOS_Auth_VerifyUserAuth);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	//currently unused:
	//EOS_Auth_VerifyUserAuthOptions options;
	//if (Options)
	//	options = *Options;

	NEOS_AddCallback([Options, ClientData, CompletionDelegate]()
		{
			EOS_Auth_VerifyUserAuthCallbackInfo cbi;
			cbi.ClientData = ClientData;
			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;

			CompletionDelegate(&cbi);
		});
}

/**
 * Fetch the number of accounts that are logged in.
 *
 * @return the number of accounts logged in.
 */
EOS_DECLARE_FUNC(int32_t) EOS_Auth_GetLoggedInAccountsCount(EOS_HAuth Handle)
{
	log(LL::Debug, "EOS_Auth_GetLoggedInAccountsCount (%p)", Handle);

	PROXY_FUNC(EOS_Auth_GetLoggedInAccountsCount);
	if (proxied)
	{
		auto res = proxied(Handle);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	return 1;
}

/**
 * Fetch an account id that is logged in.
 *
 * @param Index an index into the list of logged in accounts. If the index is out of bounds, the returned account id will be invalid.
 *
 * @return the account id associated with the index passed
 */
EOS_DECLARE_FUNC(EOS_EpicAccountId) EOS_Auth_GetLoggedInAccountByIndex(EOS_HAuth Handle, int32_t Index)
{
	log(LL::Debug, "EOS_Auth_GetLoggedInAccountByIndex (%p, %d)", Handle, Index);

	PROXY_FUNC(EOS_Auth_GetLoggedInAccountByIndex);
	if (proxied)
	{
		auto res = proxied(Handle, Index);
		log(LL::Debug, " + Proxy Result: %p", res);
		return res;
	}

	if (Index != 0)
		return nullptr;

	return UserAccountId;
}

/**
 * Fetches the login status for an account id.
 *
 * @param LocalUserId the account id of the user being queried
 *
 * @return the enum value of a user's login status
 */
EOS_DECLARE_FUNC(EOS_ELoginStatus) EOS_Auth_GetLoginStatus(EOS_HAuth Handle, EOS_EpicAccountId LocalUserId)
{
	log(LL::Debug, "EOS_Auth_GetLoginStatus (%p, %p)", Handle, LocalUserId);

	PROXY_FUNC(EOS_Auth_GetLoginStatus);
	if (proxied)
	{
		auto res = proxied(Handle, LocalUserId);
		log(LL::Debug, " + Proxy Result: %s (%d)", EnumToString(EOSEnum::ELoginStatus, (int32_t)res), res);
		return res;
	}

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_ELoginStatus::EOS_LS_NotLoggedIn;
	}

	auto* auth = reinterpret_cast<HAuth*>(Handle);

	if (LocalUserId == UserAccountId)
		return auth->GetLoginStatus();

	return EOS_ELoginStatus::EOS_LS_NotLoggedIn;
}

/**
 * Fetches a user auth token for an account id.
 *
 * @param LocalUserId the account id of the user being queried
 * @param OutUserAuthToken the auth token for the given user, if it exists and is valid, use EOS_Auth_Token_Release when finished
 *
 * @see EOS_Auth_Token_Release
 *
 * @return EOS_Success if the information is available and passed out in OutUserAuthToken
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_NotFound if the auth token is not found or expired.
 *
 */
#ifdef EOS_BUILD_120
EOS_DECLARE_FUNC(EOS_EResult) EOS_Auth_CopyUserAuthToken(EOS_HAuth Handle, const EOS_Auth_CopyUserAuthTokenOptions* Options, EOS_EpicAccountId LocalUserId, EOS_Auth_Token** OutUserAuthToken)
#else
EOS_DECLARE_FUNC(EOS_EResult) EOS_Auth_CopyUserAuthToken(EOS_HAuth Handle, EOS_EpicAccountId LocalUserId, EOS_Auth_Token** OutUserAuthToken)
#endif
{
	// TODO120: Options
	log(LL::Debug, "EOS_Auth_CopyUserAuthToken (%p, %p, %p)", Handle, LocalUserId, OutUserAuthToken);
	
#ifdef EOS_BUILD_120
	EOS_CHECK_VERSION(EOS_AUTH_COPYUSERAUTHTOKEN_API_LATEST);
#endif

	PROXY_FUNC(EOS_Auth_CopyUserAuthToken);
	if (proxied)
	{
#ifdef EOS_BUILD_120
		auto res = proxied(Handle, Options, LocalUserId, OutUserAuthToken);
#else
		auto res = proxied(Handle, LocalUserId, OutUserAuthToken);
#endif
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!OutUserAuthToken)
		return EOS_EResult::EOS_InvalidParameters;

	if (LocalUserId != UserAccountId)
		return EOS_EResult::EOS_NotFound;

	*OutUserAuthToken = new EOS_Auth_Token;
	(*OutUserAuthToken)->ApiVersion = EOS_AUTH_TOKEN_API_LATEST;
	(*OutUserAuthToken)->App = GameProductId.c_str();
	(*OutUserAuthToken)->ClientId = GameClientId.c_str();
	(*OutUserAuthToken)->AccountId = LocalUserId;
	(*OutUserAuthToken)->AccessToken = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
	(*OutUserAuthToken)->ExpiresIn = 216000.f;
	(*OutUserAuthToken)->ExpiresAt = "2030-09-15T12:00:00";
	(*OutUserAuthToken)->AuthType = EOS_EAuthTokenType::EOS_ATT_User;

	return EOS_EResult::EOS_Success;
}

/**
 * Register to receive login status updates.
 * @note must call RemoveNotifyLoginStatusChanged to remove the notification
 *
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param Notification a callback that is fired when the login status for a user changes
 *
 * @return handle representing the registered callback
 */
#ifdef EOS_BUILD_120
EOS_DECLARE_FUNC(EOS_NotificationId) EOS_Auth_AddNotifyLoginStatusChanged(EOS_HAuth Handle, const EOS_Auth_AddNotifyLoginStatusChangedOptions* Options, void* ClientData, const EOS_Auth_OnLoginStatusChangedCallback Notification)
#else
EOS_DECLARE_FUNC(EOS_NotificationId) EOS_Auth_AddNotifyLoginStatusChanged(EOS_HAuth Handle, void* ClientData, const EOS_Auth_OnLoginStatusChangedCallback Notification)
#endif
{
	// TODO120: Options
	log(LL::Debug, "EOS_Auth_AddNotifyLoginStatusChanged (%p, %p, %p)", Handle, ClientData, Notification);

#ifdef EOS_BUILD_120
	EOS_CHECK_VERSION(EOS_AUTH_ADDNOTIFYLOGINSTATUSCHANGED_API_LATEST);
#endif

	PROXY_FUNC(EOS_Auth_AddNotifyLoginStatusChanged);
	if (proxied)
	{
#ifdef EOS_BUILD_120
		auto res = proxied(Handle, Options, ClientData, Notification);
#else
		auto res = proxied(Handle, ClientData, Notification);
#endif
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_INVALID_NOTIFICATIONID;
	}

	auto* auth = reinterpret_cast<HAuth*>(Handle);

	// Note: callback body must remain part of the callback!
	auto idx = auth->LoginCallbackAdd([auth, ClientData, Notification]()
		{
			EOS_Auth_LoginStatusChangedCallbackInfo cbi;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = UserAccountId;
			cbi.PrevStatus = auth->GetPrevLoginStatus();
			cbi.CurrentStatus = auth->GetLoginStatus();
			Notification(&cbi);
		});

	return NEOS_NOTIFY_FIRSTID + idx;
}

/**
 * Unregister from receiving login status updates.
 *
 * @param InId handle representing the registered callback
 */
EOS_DECLARE_FUNC(void) EOS_Auth_RemoveNotifyLoginStatusChanged(EOS_HAuth Handle, EOS_NotificationId InId)
{
	log(LL::Debug, "EOS_Auth_RemoveNotifyLoginStatusChanged (%p, %lld)", Handle, InId);

	PROXY_FUNC(EOS_Auth_RemoveNotifyLoginStatusChanged);
	if (proxied)
	{
		proxied(Handle, InId);
		return;
	}

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return;
	}

	auto* auth = reinterpret_cast<HAuth*>(Handle);

	if (InId < NEOS_NOTIFY_FIRSTID)
	{
		log(LL::Warning, __FUNCTION__ ": invalid InId %p!", InId);
		return;
	}
	size_t index = InId - NEOS_NOTIFY_FIRSTID;
	auth->LoginCallbackRemove(index);
}
