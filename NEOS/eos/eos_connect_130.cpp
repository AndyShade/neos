#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * Retrieve the equivalent account id mappings for a list of product user ids.  The values will be cached and retrievable via EOS_Connect_GetProductUserIdMapping
 *
 * @param Options structure containing a list of product user ids to query for the external account representation
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the query operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Connect_QueryProductUserIdMappings(EOS_HConnect Handle, const EOS_Connect_QueryProductUserIdMappingsOptions* Options, void* ClientData, const EOS_Connect_OnQueryProductUserIdMappingsCallback CompletionDelegate)
{
  log(LL::Debug, __FUNCTION__ " (%p, %p, %p)", Options, ClientData, CompletionDelegate);

  EOS_CHECK_VERSION(EOS_CONNECT_QUERYPRODUCTUSERIDMAPPINGS_API_LATEST);

  PROXY_FUNC(EOS_Connect_QueryProductUserIdMappings);
  if (proxied)
  {
    proxied(Handle, Options, ClientData, CompletionDelegate);
    return;
  }

  EOS_Connect_QueryProductUserIdMappingsOptions options;
  if (Options)
  	options = *Options;

  NEOS_AddCallback([Options, options, ClientData, CompletionDelegate]()
    {
      EOS_Connect_QueryProductUserIdMappingsCallbackInfo cbi;
      cbi.ClientData = ClientData;
      cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
      cbi.LocalUserId = Options ? options.LocalUserId : 0;
      CompletionDelegate(&cbi);
    });
}

/**
 * Fetch an external account id, in string form, that maps to a given product user id
 *
 * @param Options structure containing the local user and target product user id
 * @param OutBuffer The buffer into which the account id data should be written.  The buffer must be long enough to hold a string of EOS_CONNECT_EXTERNAL_ACCOUNT_ID_MAX_LENGTH.
 * @param InOutBufferLength The size of the OutBuffer in characters.
 *                          The input buffer should include enough space to be null-terminated.
 *                          When the function returns, this parameter will be filled with the length of the string copied into OutBuffer.
 *
 * @return An EOS_EResult that indicates the external account id was copied into the OutBuffer
 *         EOS_Success if the information is available and passed out in OutUserInfo
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_NotFound if the mapping doesn't exist or hasn't been queried yet
 *         EOS_LimitExceeded - The OutBuffer is not large enough to receive the external account id. InOutBufferLength contains the required minimum length to perform the operation successfully.
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Connect_GetProductUserIdMapping(EOS_HConnect Handle, const EOS_Connect_GetProductUserIdMappingOptions* Options, char* OutBuffer, int32_t* InOutBufferLength)
{
  if(InOutBufferLength)
    log(LL::Debug, __FUNCTION__ " (%p, %p, %p(%d))", Options, OutBuffer, InOutBufferLength, *InOutBufferLength);
  else
    log(LL::Debug, __FUNCTION__ " (%p, %p, %p)", Options, OutBuffer, InOutBufferLength);

  EOS_CHECK_VERSION(EOS_CONNECT_GETPRODUCTUSERIDMAPPING_API_LATEST);

  PROXY_FUNC(EOS_Connect_GetProductUserIdMapping);
  if (proxied)
  {
    auto res = proxied(Handle, Options, OutBuffer, InOutBufferLength);
    log(LL::Debug, " + Proxy Result: %d", res);
    return res;
  }

  EOS_CHECK_CONFIGURED();

  // TODO!
  return EOS_EResult::EOS_NotFound;
}
