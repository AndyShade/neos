#include "../stdafx.h"
#include "../NEOS.hpp"

inline char* TryAllocMemory(size_t Size)
{
	if (GameAllocMemoryFunc)
		return static_cast<char*>(GameAllocMemoryFunc(Size, 1));
	if (ForcedDLCUseMalloc)
		return static_cast<char*>(malloc(Size));

	return nullptr;
}

/**
 * Release the memory associated with an EOS_Ecom_Entitlement structure. This must be called on data retrieved from EOS_Ecom_Entitlement_Release.
 *
 * @param Entitlement - The entitlement structure to be released
 *
 * @see EOS_Ecom_Entitlement
 * @see EOS_Ecom_CopyEntitlementByIndex
 * @see EOS_Ecom_CopyEntitlementById
 */
EOS_DECLARE_FUNC(void) EOS_Ecom_Entitlement_Release(EOS_Ecom_Entitlement* Entitlement)
{
	log(LL::Debug, "EOS_Ecom_Entitlement_Release");

	// No proxying since we always handle entitlements
	delete Entitlement;
}

/**
 * The Ecom Interface exposes all catalog, purchasing, and ownership entitlement features available with the Epic Games store
 * All Ecom Interface calls take a handle of type EOS_HEcom as the first parameter.
 * This handle can be retrieved from a EOS_HPlatform handle by using the EOS_Platform_GetEcomInterface function.
 *
 * @see EOS_Platform_GetEcomInterface
 */

 /**
  * Query the ownership status for a given list of catalog item ids defined with Epic Online Services.
  * This data will be cached for a limited time and retrieved again from the backend when necessary
  *
  * @param Options structure containing the account and catalog item ids to retrieve
  * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
  * @param CompletionDelegate a callback that is fired when the async operation completes, either successfully or in error
  */

EOS_DECLARE_FUNC(void) EOS_Ecom_QueryOwnership(EOS_HEcom Handle, const EOS_Ecom_QueryOwnershipOptions* Options, void* ClientData, const EOS_Ecom_OnQueryOwnershipCallback CompletionDelegate)
{
	if (DLCLogQueries)
	{
		if (Options)
			log(LL::Info, "EOS_Ecom_QueryOwnership (%p(%d items), %p, %p)", Options, Options->CatalogItemIdCount, ClientData, CompletionDelegate);
		else
			log(LL::Info, "EOS_Ecom_QueryOwnership (%p, %p, %p)", Options, ClientData, CompletionDelegate);
	}

	EOS_CHECK_VERSION(EOS_ECOM_QUERYOWNERSHIP_API_LATEST);

	EOS_Ecom_QueryOwnershipOptions options;
	EOS_Ecom_CatalogItemId catalogItemIds[EOS_ECOM_QUERYOWNERSHIPTOKEN_MAX_CATALOGITEM_IDS];

	// Make a copy of Options and CatalogItemIds, as some games will free it straight after this function
	if (Options)
	{
		options = *Options;

		// Have to copy this array seperately :(
		// Note: can't set options.CatalogItemIds to point to catalogItemIds, as the callback capture below will relocate catalogItemIds!
		for (uint32_t i = 0; i < Options->CatalogItemIdCount; i++)
			catalogItemIds[i] = Options->CatalogItemIds[i];

		options.CatalogItemIds = nullptr; // do not use!
	}

	NEOS_AddCallback([options, catalogItemIds, Options, ClientData, CompletionDelegate]()
		{
			EOS_Ecom_QueryOwnershipCallbackInfo cbi;
			EOS_Ecom_ItemOwnership ownerships[EOS_ECOM_QUERYOWNERSHIPTOKEN_MAX_CATALOGITEM_IDS];

			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;

			if (Options)
			{
				cbi.LocalUserId = options.LocalUserId;

				// Update our return vector with provided DLCs...
				fifo_map<std::string, std::tuple<const char*, EOS_EOwnershipStatus>> retVal;

				uint32_t numItems = std::min(options.CatalogItemIdCount, (uint32_t)EOS_ECOM_QUERYOWNERSHIPTOKEN_MAX_CATALOGITEM_IDS);
				for (uint32_t i = 0; i < numItems; i++)
				{
					std::string itemId = catalogItemIds[i];
					bool owned = DLCAllOwned || DLCOwned.count(itemId) > 0 || DLCForced.count(itemId) > 0;

					retVal[itemId] = std::tuple<const char*, EOS_EOwnershipStatus>(catalogItemIds[i], owned ? EOS_EOwnershipStatus::EOS_OS_Owned : EOS_EOwnershipStatus::EOS_OS_NotOwned);
				}

				// Try adding any forced DLCs
				// Will only work if game provided AllocateMemoryFunction or ForcedDLC->UseMalloc is set!
				for (auto kvp : DLCForced)
				{
					if (retVal.count(kvp.first) && std::get<0>(retVal[kvp.first]) != nullptr)
					{
						// Already added by the loop above or a prior forced DLC, no need to reallocate
						retVal[kvp.first] = std::tuple<const char*, EOS_EOwnershipStatus>(std::get<0>(retVal[kvp.first]), EOS_EOwnershipStatus::EOS_OS_Owned);
					}
					else
					{
						char* idBuffer = TryAllocMemory(kvp.first.length() + 1);
						if (idBuffer)
						{
							memcpy(idBuffer, kvp.first.c_str(), kvp.first.length());
							idBuffer[kvp.first.length()] = 0;

							retVal[kvp.first] = std::tuple<const char*, EOS_EOwnershipStatus>(idBuffer, EOS_EOwnershipStatus::EOS_OS_Owned);
						}
						else
						{
							static bool NotifiedUser = false;
							if (!NotifiedUser)
							{
								log(LL::Warning, " - (game didn't provide memory allocation function - maybe try setting ForcedDLC->UseMalloc to 1)");
								NotifiedUser = true;
							}

							log(LL::Error, " - Failed to add forced DLC %s as we couldn't allocate memory for it...", kvp.first.c_str());
						}
					}
				}

				// Finally update the EOS_Ecom_ItemOwnership array with our vector contents
				int i = 0;
				for (auto kvp : retVal)
				{
					if (i >= EOS_ECOM_QUERYOWNERSHIPTOKEN_MAX_CATALOGITEM_IDS)
						break;

					if (DLCLogQueries)
					{
						std::string dlcName;
						if (DLCOwned.count(kvp.first) > 0)
							dlcName = DLCOwned[kvp.first];
						else if (DLCForced.count(kvp.first) > 0)
							dlcName = DLCForced[kvp.first];

						if (dlcName.length())
							log(LL::Info, " - (%s) %s (%s)", std::get<1>(kvp.second) == EOS_EOwnershipStatus::EOS_OS_Owned ? "Owned" : "Unowned", kvp.first.c_str(), dlcName.c_str());
						else
							log(LL::Info, " - (%s) %s", std::get<1>(kvp.second) == EOS_EOwnershipStatus::EOS_OS_Owned ? "Owned" : "Unowned", kvp.first.c_str());
					}

					ownerships[i].ApiVersion = options.ApiVersion;
					ownerships[i].Id = std::get<0>(kvp.second);
					ownerships[i].OwnershipStatus = std::get<1>(kvp.second);

					i++;
				}

				cbi.ItemOwnershipCount = i;
				cbi.ItemOwnership = ownerships;
			}
			else
			{
				cbi.LocalUserId = nullptr;
				cbi.ItemOwnership = nullptr;
				cbi.ItemOwnershipCount = 0;
			}
			CompletionDelegate(&cbi);

			// Shouldn't need to de-alloc memory, game should take care of it
		});
}

/**
 * Query the ownership status for a given list of catalog item ids defined with Epic Online Services.
 * The data is return via the callback in the form of a signed JWT that should be verified by an external backend server using a public key for authenticity.
 *
 * @param Options structure containing the account and catalog item ids to retrieve in token form
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the async operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Ecom_QueryOwnershipToken(EOS_HEcom Handle, const EOS_Ecom_QueryOwnershipTokenOptions* Options, void* ClientData, const EOS_Ecom_OnQueryOwnershipTokenCallback CompletionDelegate)
{
	if (Options)
		log(LL::Debug, "EOS_Ecom_QueryOwnershipToken (%p(%p), %p, %p)", Options, Options->LocalUserId, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Ecom_QueryOwnershipToken (%p, %p, %p)", Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_ECOM_QUERYOWNERSHIPTOKEN_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_QueryOwnershipToken);
	if (proxied)
	{
		proxied(ecom->GetProxyHandle(), Options, ClientData, CompletionDelegate);
		return;
	}

	// Make a copy of Options as some games will free it straight after this function
	EOS_Ecom_QueryOwnershipTokenOptions options;
	if (Options)
		options = *Options;

	NEOS_AddCallback([options, Options, ClientData, CompletionDelegate]()
		{
			EOS_Ecom_QueryOwnershipTokenCallbackInfo cbi;
			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = Options ? options.LocalUserId : nullptr;
			cbi.OwnershipToken = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

			CompletionDelegate(&cbi);
		});
}
