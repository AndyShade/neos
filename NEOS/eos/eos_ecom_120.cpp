#include "../stdafx.h"
#include "../NEOS.hpp"

// New ECom stuff that was added in 1.2.0:

/**
 * Release the memory associated with an EOS_Ecom_CatalogItem structure. This must be called on data retrieved from EOS_Ecom_CopyOfferItemByIndex.
 *
 * @param CatalogItem - The catalog item structure to be released
 *
 * @see EOS_Ecom_CatalogItem
 * @see EOS_Ecom_CopyOfferItemByIndex
 */
EOS_DECLARE_FUNC(void) EOS_Ecom_CatalogItem_Release(EOS_Ecom_CatalogItem* CatalogItem)
{
	log(LL::Debug, "EOS_Ecom_CatalogItem_Release (%p)", CatalogItem);

	PROXY_FUNC(EOS_Ecom_CatalogItem_Release);
	if (proxied)
	{
		proxied(CatalogItem);
		return;
	}

	delete CatalogItem;
}

/**
 * Release the memory associated with an EOS_Ecom_CatalogOffer structure. This must be called on data retrieved from EOS_Ecom_CopyOfferByIndex.
 *
 * @param CatalogOffer - The catalog offer structure to be released
 *
 * @see EOS_Ecom_CatalogOffer
 * @see EOS_Ecom_CopyOfferByIndex
 */
EOS_DECLARE_FUNC(void) EOS_Ecom_CatalogOffer_Release(EOS_Ecom_CatalogOffer* CatalogOffer)
{
	log(LL::Debug, "EOS_Ecom_CatalogOffer_Release (%p)", CatalogOffer);

	PROXY_FUNC(EOS_Ecom_CatalogOffer_Release);
	if (proxied)
	{
		proxied(CatalogOffer);
		return;
	}

	delete CatalogOffer;
}

/**
 * Release the memory associated with an EOS_Ecom_KeyImageInfo structure. This must be called on data retrieved from EOS_Ecom_CopyItemImageByIndex.
 *
 * @param KeyImageInfo - The key image info structure to be released
 *
 * @see EOS_Ecom_KeyImageInfo
 * @see EOS_Ecom_CopyItemImageInfoByIndex
 */
EOS_DECLARE_FUNC(void) EOS_Ecom_KeyImageInfo_Release(EOS_Ecom_KeyImageInfo* KeyImageInfo)
{
	log(LL::Debug, "EOS_Ecom_KeyImageInfo_Release (%p)", KeyImageInfo);

	PROXY_FUNC(EOS_Ecom_KeyImageInfo_Release);
	if (proxied)
	{
		proxied(KeyImageInfo);
		return;
	}

	delete KeyImageInfo;
}

/**
 * Release the memory associated with an EOS_Ecom_CatalogRelease structure. This must be called on data retrieved from EOS_Ecom_CopyItemReleaseByIndex.
 *
 * @param CatalogRelease - The catalog release structure to be released
 *
 * @see EOS_Ecom_CatalogRelease
 * @see EOS_Ecom_CopyItemReleaseByIndex
 */
EOS_DECLARE_FUNC(void) EOS_Ecom_CatalogRelease_Release(EOS_Ecom_CatalogRelease* CatalogRelease)
{
	log(LL::Debug, "EOS_Ecom_CatalogRelease_Release (%p)", CatalogRelease);

	PROXY_FUNC(EOS_Ecom_CatalogRelease_Release);
	if (proxied)
	{
		proxied(CatalogRelease);
		return;
	}

	delete CatalogRelease;
}

/**
 * Release the memory associated with an EOS_Ecom_HTransaction.  Is is expected to be called after being received from a EOS_Ecom_CheckoutCallbackInfo.
 *
 * @param Transaction A handle to a transaction.
 *
 * @see EOS_Ecom_CheckoutCallbackInfo
 * @see EOS_Ecom_GetTransactionCount
 * @see EOS_Ecom_GetTransactionByIndex
 */
EOS_DECLARE_FUNC(void) EOS_Ecom_Transaction_Release(EOS_Ecom_HTransaction Transaction)
{
	log(LL::Debug, "EOS_Ecom_Transaction_Release (%p)", Transaction);

	PROXY_FUNC(EOS_Ecom_Transaction_Release);
	if (proxied)
	{
		proxied(Transaction);
		return;
	}

	//TODOSTUB
	//delete Transaction;
}

/**
 * Query the entitlement information for a given list of entitlement names defined with Epic Online Services.
 * This data will be cached for a limited time and retrieved again from the backend when necessary.
 * Use EOS_Ecom_CopyEntitlementByIndex and EOS_Ecom_CopyEntitlementById to get the entitlement details.
 * If no entitlement IDs are provided then all entitlements for the associated account are returned within the provided page.
 *
 * @param Options structure containing the account and entitlement names to retrieve
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the async operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Ecom_QueryEntitlements(EOS_HEcom Handle, const EOS_Ecom_QueryEntitlementsOptions* Options, void* ClientData, const EOS_Ecom_OnQueryEntitlementsCallback CompletionDelegate)
{
	if (DLCLogQueries)
	{
		if (Options)
			log(LL::Info, "EOS_Ecom_QueryEntitlements (%p(%d items, %d), %p, %p)", Options, Options->EntitlementIdCount, Options->bIncludeRedeemed, ClientData, CompletionDelegate);
		else
			log(LL::Info, "EOS_Ecom_QueryEntitlements (%p, %p, %p)", Options, ClientData, CompletionDelegate);
	}

	EOS_CHECK_VERSION(EOS_ECOM_QUERYENTITLEMENTS_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return;
	}

	EOS_Ecom_QueryEntitlementsOptions options;
	EOS_Ecom_EntitlementId entitlementIds[EOS_ECOM_QUERYENTITLEMENTS_MAX_ENTITLEMENT_IDS];

	// Make a copy of Options and EntitlementIds, as some games will free it straight after this function
	if (Options)
	{
		options = *Options;

		// Have to copy this array seperately :(
		// Note: can't set options.CatalogItemIds to point to catalogItemIds, as the callback capture below will relocate catalogItemIds!
		for (uint32_t i = 0; i < Options->EntitlementIdCount; i++)
			entitlementIds[i] = Options->EntitlementIds[i];

		options.EntitlementIds = nullptr; // do not use!
	}

	NEOS_AddCallback([Handle, options, entitlementIds, Options, ClientData, CompletionDelegate]()
		{
			auto* ecom = reinterpret_cast<HEcom*>(Handle);

			EOS_Ecom_QueryEntitlementsCallbackInfo cbi;

			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;

			if (Options)
			{
				cbi.LocalUserId = options.LocalUserId;

				// go through each requested entitlement, and look inside our Entitlements list for matching ones, and add them to HEcom
				for (uint32_t i = 0; i < options.EntitlementIdCount; i++)
				{
					auto& eid = entitlementIds[i];

					int numOwned = 0;
					for(size_t i = 0; i < Entitlements.size(); i++)
					{
						auto& pair = Entitlements.at(i);
						if (pair.first != eid)
							continue;

						// we have an instance!
						ecom->CacheEntitlement(i);
						numOwned++;
					}

					if (DLCLogQueries)
						log(LL::Info, " - %s (%d owned)", eid, numOwned);
				}
			}

			CompletionDelegate(&cbi);
		});
	// TODO120
}

/**
 * Query for a list of catalog offers defined with Epic Online Services.
 * This data will be cached for a limited time and retrieved again from the backend when necessary.
 *
 * @param Options structure containing filter criteria
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the async operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Ecom_QueryOffers(EOS_HEcom Handle, const EOS_Ecom_QueryOffersOptions* Options, void* ClientData, const EOS_Ecom_OnQueryOffersCallback CompletionDelegate)
{
	if (Options)
	{
		if (Options->OverrideCatalogNamespace)
			log(LL::Debug, "EOS_Ecom_QueryOffers (%p, %p(%d, %p, %p(%s)), %p, %p)", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->OverrideCatalogNamespace, Options->OverrideCatalogNamespace, ClientData, CompletionDelegate);
		else
			log(LL::Debug, "EOS_Ecom_QueryOffers (%p, %p(%d, %p, %p), %p, %p)", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->OverrideCatalogNamespace, ClientData, CompletionDelegate);
	}
	else
		log(LL::Debug, "EOS_Ecom_QueryOffers (%p, %p, %p, %p)", Handle, Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_ECOM_QUERYOFFERS_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_QueryOffers);
	if (proxied)
	{
		proxied(ecom->GetProxyHandle(), Options, ClientData, CompletionDelegate);
		return;
	}

	// TODOASYNC
	return;
}

/**
 * Initiates the purchase flow for a set of offers.  The callback is triggered after the purchase flow.
 * On success, the set of entitlements that were unlocked will be cached.
 * On success, a Transaction Id will be returned. The Transaction Id can be used to obtain a
 * EOS_Ecom_HTransaction handle. The handle can then be used to retrieve the entitlements rewarded by the purchase.
 *
 * @see EOS_Ecom_Transaction_Release
 *
 * @param Options structure containing filter criteria
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the async operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Ecom_Checkout(EOS_HEcom Handle, const EOS_Ecom_CheckoutOptions* Options, void* ClientData, const EOS_Ecom_OnCheckoutCallback CompletionDelegate)
{
	if (Options)
	{
		if (Options->OverrideCatalogNamespace)
			log(LL::Debug, "EOS_Ecom_Checkout (%p, %p(%d, %p, %p(%s), %p, %p), %p, %p)", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->OverrideCatalogNamespace, Options->OverrideCatalogNamespace, Options->EntryCount, Options->Entries, ClientData, CompletionDelegate);
		else
			log(LL::Debug, "EOS_Ecom_Checkout (%p, %p(%d, %p, %p, %p, %p), %p, %p)", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->OverrideCatalogNamespace, Options->EntryCount, Options->Entries, ClientData, CompletionDelegate);
	}
	else
		log(LL::Debug, "EOS_Ecom_Checkout (%p, %p, %p, %p)", Handle, Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_ECOM_CHECKOUT_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_Checkout);
	if (proxied)
	{
		proxied(ecom->GetProxyHandle(), Options, ClientData, CompletionDelegate);
		return;
	}

	// TODOASYNC
	return;
}

/**
 * Requests that the provided entitlement be marked redeemed.  This will cause that entitlement
 * to no longer be returned from QueryEntitlements unless the include redeemed request flag is set true.
 *
 * @param Options structure containing entitlement to redeem
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the async operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Ecom_RedeemEntitlements(EOS_HEcom Handle, const EOS_Ecom_RedeemEntitlementsOptions* Options, void* ClientData, const EOS_Ecom_OnRedeemEntitlementsCallback CompletionDelegate)
{
	if (Options)
		log(LL::Debug, "EOS_Ecom_RedeemEntitlements (%p, %p(%d, %p, %d, %p), %p, %p)", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->EntitlementInstanceIdCount, Options->EntitlementInstanceIds, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Ecom_RedeemEntitlements (%p, %p, %p, %p)", Handle, Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_ECOM_REDEEMENTITLEMENTS_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_RedeemEntitlements);
	if (proxied)
	{
		proxied(ecom->GetProxyHandle(), Options, ClientData, CompletionDelegate);
		return;
	}

	// TODOASYNC
	return;
}

/**
 * Fetch the number of entitlements that are cached locally for a given local user.
 *
 * @param Options structure containing the account id being accessed
 *
 * @see EOS_Ecom_CopyEntitlementByIndex
 *
 * @return the number of entitlements found.
 */
EOS_DECLARE_FUNC(uint32_t) EOS_Ecom_GetEntitlementsCount(EOS_HEcom Handle, const EOS_Ecom_GetEntitlementsCountOptions* Options)
{
	log(LL::Debug, "EOS_Ecom_GetEntitlementsCount (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_GETENTITLEMENTSCOUNT_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return 0;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	return static_cast<uint32_t>(ecom->GetEntitlementCount());
}

/**
 * Fetches an entitlement from a given index.
 *
 * @param Options structure containing the account id and index being accessed
 * @param OutEntitlement the entitlement for the given index, if it exists and is valid, use EOS_Ecom_Entitlement_Release when finished
 *
 * @see EOS_Ecom_Entitlement_Release
 *
 * @return EOS_Success if the information is available and passed out in OutEntitlement
 *         EOS_Ecom_EntitlementStale if the entitlement information is stale and passed out in OutEntitlement
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_NotFound if the entitlement is not found
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Ecom_CopyEntitlementByIndex(EOS_HEcom Handle, const EOS_Ecom_CopyEntitlementByIndexOptions* Options, EOS_Ecom_Entitlement ** OutEntitlement)
{
	if (Options)
		log(LL::Debug, "EOS_Ecom_CopyEntitlementByIndex (%p(%d, %p, %d), %p)", Options, Options->ApiVersion, Options->LocalUserId, Options->EntitlementIndex, OutEntitlement);
	else
		log(LL::Debug, "EOS_Ecom_CopyEntitlementByIndex (%p, %p)", Options, OutEntitlement);

	EOS_CHECK_VERSION(EOS_ECOM_COPYENTITLEMENTBYINDEX_API_LATEST);

	EOS_CHECK_CONFIGURED();

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_EResult::EOS_InvalidParameters;
	}

	if (!Options || !OutEntitlement)
		return EOS_EResult::EOS_InvalidParameters;

	auto* ecom = reinterpret_cast<HEcom*>(Handle);

	*OutEntitlement = nullptr;

	auto* ent = ecom->GetEntitlementByIndex(Options->EntitlementIndex);
	if (!ent)
		return EOS_EResult::EOS_NotFound;

	*OutEntitlement = new EOS_Ecom_Entitlement(*ent);

	return EOS_EResult::EOS_Success;	
}

/**
 * Fetches an entitlement with a given ID.
 *
 * @param Options structure containing the account id and entitlement id being accessed
 * @param OutEntitlement the entitlement for the given id, if it exists and is valid, use EOS_Ecom_Entitlement_Release when finished
 *
 * @see EOS_Ecom_Entitlement_Release
 *
 * @return EOS_Success if the information is available and passed out in OutEntitlement
 *         EOS_Ecom_EntitlementStale if the entitlement information is stale and passed out in OutEntitlement
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_NotFound if the entitlement is not found
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Ecom_CopyEntitlementById(EOS_HEcom Handle, const EOS_Ecom_CopyEntitlementByIdOptions* Options, EOS_Ecom_Entitlement ** OutEntitlement)
{
	if (Options)
		log(LL::Debug, "EOS_Ecom_CopyEntitlementById (%p(%d, %p, %p(%s)), %p)", Options, Options->ApiVersion, Options->LocalUserId, Options->EntitlementId, Options->EntitlementId, OutEntitlement);
	else
		log(LL::Debug, "EOS_Ecom_CopyEntitlementById (%p, %p)", Options, OutEntitlement);

	EOS_CHECK_VERSION(EOS_ECOM_COPYENTITLEMENTBYID_API_LATEST);

	EOS_CHECK_CONFIGURED();

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_EResult::EOS_InvalidParameters;
	}

	if (!Options || !OutEntitlement)
		return EOS_EResult::EOS_InvalidParameters;

	auto* ecom = reinterpret_cast<HEcom*>(Handle);

	*OutEntitlement = nullptr;

	auto* ent = ecom->GetEntitlementByEntitlementId(Options->EntitlementId);
	if (!ent)
		return EOS_EResult::EOS_NotFound;

	*OutEntitlement = new EOS_Ecom_Entitlement(*ent);

	return EOS_EResult::EOS_Success;
}

/**
 * Fetch the number of offers that are cached locally for a given local user.
 *
 * @param Options structure containing the account id being accessed
 *
 * @see EOS_Ecom_CopyOfferByIndex
 *
 * @return the number of offers found.
 */
EOS_DECLARE_FUNC(uint32_t) EOS_Ecom_GetOfferCount(EOS_HEcom Handle, const EOS_Ecom_GetOfferCountOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_Ecom_GetOfferCount (%p, %p(%d, %p))", Handle, Options, Options->ApiVersion, Options->LocalUserId);
	else
		log(LL::Debug, "EOS_Ecom_GetOfferCount (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_GETOFFERCOUNT_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return 0;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_GetOfferCount);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	// TODOSTUB
	return 0;
}

/**
 * Fetches an offer from a given index.  The pricing and text are localized to the provided account.
 *
 * @param Options structure containing the account id and index being accessed
 * @param OutOffer the offer for the given index, if it exists and is valid, use EOS_Ecom_CatalogOffer_Release when finished
 *
 * @see EOS_Ecom_CatalogOffer_Release
 * @see EOS_Ecom_GetOfferItemCount
 *
 * @return EOS_Success if the information is available and passed out in OutOffer
 *         EOS_Ecom_CatalogOfferStale if the offer information is stale and passed out in OutOffer
 *         EOS_Ecom_CatalogOfferPriceInvalid if the offer information has an invalid price and passed out in OutOffer
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_NotFound if the offer is not found
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Ecom_CopyOfferByIndex(EOS_HEcom Handle, const EOS_Ecom_CopyOfferByIndexOptions* Options, EOS_Ecom_CatalogOffer ** OutOffer)
{
	if (Options)
		log(LL::Debug, "EOS_Ecom_CopyOfferByIndex (%p, %p(%d, %p, %d))", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->OfferIndex);
	else
		log(LL::Debug, "EOS_Ecom_CopyOfferByIndex (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_COPYOFFERBYINDEX_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_EResult::EOS_InvalidParameters;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_CopyOfferByIndex);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options, OutOffer);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!OutOffer)
		return EOS_EResult::EOS_InvalidParameters;

	*OutOffer = nullptr;

	if(!Options)
		return EOS_EResult::EOS_InvalidParameters;

	// TODOSTUB
	return EOS_EResult::EOS_NotFound;
}

/**
 * Fetches an offer with a given ID.  The pricing and text are localized to the provided account.
 *
 * @param Options structure containing the account id and offer id being accessed
 * @param OutOffer the offer for the given index, if it exists and is valid, use EOS_Ecom_CatalogOffer_Release when finished
 *
 * @see EOS_Ecom_CatalogOffer_Release
 * @see EOS_Ecom_GetOfferItemCount
 *
 * @return EOS_Success if the information is available and passed out in OutOffer
 *         EOS_Ecom_CatalogOfferStale if the offer information is stale and passed out in OutOffer
 *         EOS_Ecom_CatalogOfferPriceInvalid if the offer information has an invalid price and passed out in OutOffer
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_NotFound if the offer is not found
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Ecom_CopyOfferById(EOS_HEcom Handle, const EOS_Ecom_CopyOfferByIdOptions* Options, EOS_Ecom_CatalogOffer ** OutOffer)
{
	if (Options && Options->OfferId)
		log(LL::Debug, "EOS_Ecom_CopyOfferById (%p, %p(%d, %p, %p(%s)))", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->OfferId, Options->OfferId);
	else
		log(LL::Debug, "EOS_Ecom_CopyOfferById (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_COPYOFFERBYID_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_EResult::EOS_InvalidParameters;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_CopyOfferById);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options, OutOffer);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!OutOffer)
		return EOS_EResult::EOS_InvalidParameters;

	*OutOffer = nullptr;

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	// TODOSTUB
	return EOS_EResult::EOS_NotFound;
}

/**
 * Fetch the number of items that are associated with a given cached offer for a local user.
 *
 * @return the number of items found.
 */
EOS_DECLARE_FUNC(uint32_t) EOS_Ecom_GetOfferItemCount(EOS_HEcom Handle, const EOS_Ecom_GetOfferItemCountOptions* Options)
{
	if (Options && Options->OfferId)
		log(LL::Debug, "EOS_Ecom_GetOfferItemCount (%p, %p(%d, %p, %p(%s)))", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->OfferId, Options->OfferId);
	else
		log(LL::Debug, "EOS_Ecom_GetOfferItemCount (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_GETOFFERITEMCOUNT_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return 0;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_GetOfferItemCount);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	// TODOSTUB
	return 0;
}

/**
 * Fetches an item from a given index.
 *
 * @param Options structure containing the account id and index being accessed
 * @param OutItem the item for the given index, if it exists and is valid, use EOS_Ecom_CatalogItem_Release when finished
 *
 * @see EOS_Ecom_CatalogItem_Release
 * @see EOS_Ecom_GetItemImageInfoCount
 * @see EOS_Ecom_GetItemReleaseCount
 *
 * @return EOS_Success if the information is available and passed out in OutItem
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_Ecom_CatalogItemState if the item information is stale
 *         EOS_NotFound if the item is not found
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Ecom_CopyOfferItemByIndex(EOS_HEcom Handle, const EOS_Ecom_CopyOfferItemByIndexOptions* Options, EOS_Ecom_CatalogItem ** OutItem)
{
	if (Options && Options->OfferId)
		log(LL::Debug, "EOS_Ecom_CopyOfferItemByIndex (%p, %p(%d, %p, %p(%s), %d))", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->OfferId, Options->OfferId, Options->ItemIndex);
	else
		log(LL::Debug, "EOS_Ecom_CopyOfferItemByIndex (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_COPYOFFERITEMBYINDEX_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_EResult::EOS_InvalidParameters;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_CopyOfferItemByIndex);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options, OutItem);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!OutItem)
		return EOS_EResult::EOS_InvalidParameters;

	*OutItem = nullptr;

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	// TODOSTUB
	return EOS_EResult::EOS_NotFound;
}

/**
 * Fetches an item with a given ID.
 *
 * @param Options structure containing the item id being accessed
 * @param OutItem the item for the given index, if it exists and is valid, use EOS_Ecom_CatalogItem_Release when finished
 *
 * @see EOS_Ecom_CatalogItem_Release
 * @see EOS_Ecom_GetItemImageInfoCount
 * @see EOS_Ecom_GetItemReleaseCount
 *
 * @return EOS_Success if the information is available and passed out in OutItem
 *         EOS_Ecom_CatalogItemState if the item information is stale and passed out in OutItem
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_NotFound if the offer is not found
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Ecom_CopyItemById(EOS_HEcom Handle, const EOS_Ecom_CopyItemByIdOptions* Options, EOS_Ecom_CatalogItem ** OutItem)
{
	if (Options && Options->ItemId)
		log(LL::Debug, "EOS_Ecom_CopyItemById (%p, %p(%d, %p, %p(%s)))", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->ItemId, Options->ItemId);
	else
		log(LL::Debug, "EOS_Ecom_CopyItemById (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_COPYITEMBYID_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_EResult::EOS_InvalidParameters;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_CopyItemById);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options, OutItem);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!OutItem)
		return EOS_EResult::EOS_InvalidParameters;

	*OutItem = nullptr;

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	// TODOSTUB
	return EOS_EResult::EOS_NotFound;
}

/**
 * Fetch the number of images that are associated with a given cached item for a local user.
 *
 * @return the number of images found.
 */
EOS_DECLARE_FUNC(uint32_t) EOS_Ecom_GetItemImageInfoCount(EOS_HEcom Handle, const EOS_Ecom_GetItemImageInfoCountOptions* Options)
{
	if (Options && Options->ItemId)
		log(LL::Debug, "EOS_Ecom_GetItemImageInfoCount (%p, %p(%d, %p, %p(%s)))", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->ItemId, Options->ItemId);
	else
		log(LL::Debug, "EOS_Ecom_GetItemImageInfoCount (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_GETITEMIMAGEINFOCOUNT_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return 0;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_GetItemImageInfoCount);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	// TODOSTUB
	return 0;
}

/**
 * Fetches an image from a given index.
 *
 * @param Options structure containing the item id and index being accessed
 * @param OutImageInfo the image for the given index, if it exists and is valid, use EOS_Ecom_KeyImageInfo_Release when finished
 *
 * @see EOS_Ecom_KeyImageInfo_Release
 *
 * @return EOS_Success if the information is available and passed out in OutImageInfo
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_Ecom_CatalogItemState if the associated item information is stale
 *         EOS_NotFound if the image is not found
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Ecom_CopyItemImageInfoByIndex(EOS_HEcom Handle, const EOS_Ecom_CopyItemImageInfoByIndexOptions* Options, EOS_Ecom_KeyImageInfo ** OutImageInfo)
{
	if (Options && Options->ItemId)
		log(LL::Debug, "EOS_Ecom_CopyItemImageInfoByIndex (%p, %p(%d, %p, %p(%s), %d))", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->ItemId, Options->ItemId, Options->ImageInfoIndex);
	else
		log(LL::Debug, "EOS_Ecom_CopyItemImageInfoByIndex (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_COPYITEMIMAGEINFOBYINDEX_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_EResult::EOS_InvalidParameters;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_CopyItemImageInfoByIndex);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options, OutImageInfo);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!OutImageInfo)
		return EOS_EResult::EOS_InvalidParameters;

	*OutImageInfo = nullptr;

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	// TODOSTUB
	return EOS_EResult::EOS_NotFound;
}

/**
 * Fetch the number of releases that are associated with a given cached item for a local user.
 *
 * @return the number of releases found.
 */
EOS_DECLARE_FUNC(uint32_t) EOS_Ecom_GetItemReleaseCount(EOS_HEcom Handle, const EOS_Ecom_GetItemReleaseCountOptions* Options)
{
	if (Options && Options->ItemId)
		log(LL::Debug, "EOS_Ecom_GetItemReleaseCount (%p, %p(%d, %p, %p(%s)))", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->ItemId, Options->ItemId);
	else
		log(LL::Debug, "EOS_Ecom_GetItemReleaseCount (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_GETITEMRELEASECOUNT_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return 0;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_GetItemReleaseCount);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	// TODOSTUB
	return 0;
}

/**
 * Fetches a release from a given index.
 *
 * @param Options structure containing the item id and index being accessed
 * @param OutRelease the release for the given index, if it exists and is valid, use EOS_Ecom_CatalogRelease_Release when finished
 *
 * @see EOS_Ecom_CatalogRelease_Release
 *
 * @return EOS_Success if the information is available and passed out in OutRelease
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_Ecom_CatalogItemState if the associated item information is stale
 *         EOS_NotFound if the release is not found
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Ecom_CopyItemReleaseByIndex(EOS_HEcom Handle, const EOS_Ecom_CopyItemReleaseByIndexOptions* Options, EOS_Ecom_CatalogRelease ** OutRelease)
{
	if (Options && Options->ItemId)
		log(LL::Debug, "EOS_Ecom_CopyItemReleaseByIndex (%p, %p(%d, %p, %p(%s), %d))", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->ItemId, Options->ItemId, Options->ReleaseIndex);
	else
		log(LL::Debug, "EOS_Ecom_CopyItemReleaseByIndex (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_COPYITEMRELEASEBYINDEX_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_EResult::EOS_InvalidParameters;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_CopyItemReleaseByIndex);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options, OutRelease);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!OutRelease)
		return EOS_EResult::EOS_InvalidParameters;

	*OutRelease = nullptr;

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	// TODOSTUB
	return EOS_EResult::EOS_NotFound;
}

/**
 * Fetch the number of transactions that are cached locally for a given local user.
 *
 * @see EOS_Ecom_CheckoutCallbackInfo
 * @see EOS_Ecom_GetTransactionByIndex
 *
 * @return the number of transactions found.
 */
EOS_DECLARE_FUNC(uint32_t) EOS_Ecom_GetTransactionCount(EOS_HEcom Handle, const EOS_Ecom_GetTransactionCountOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_Ecom_GetTransactionCount (%p, %p(%d, %p))", Handle, Options, Options->ApiVersion, Options->LocalUserId);
	else
		log(LL::Debug, "EOS_Ecom_GetTransactionCount (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_GETTRANSACTIONCOUNT_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return 0;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_GetTransactionCount);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	// TODOSTUB
	return 0;
}

/**
 * Fetches the transaction handle at the given index.
 *
 * @param Options structure containing the account id and index being accessed
 *
 * @see EOS_Ecom_CheckoutCallbackInfo
 * @see EOS_Ecom_Transaction_Release
 *
 * @return EOS_Success if the information is available and passed out in OutTransaction
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_NotFound if the transaction is not found
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Ecom_CopyTransactionByIndex(EOS_HEcom Handle, const EOS_Ecom_CopyTransactionByIndexOptions* Options, EOS_Ecom_HTransaction* OutTransaction)
{
	if (Options)
		log(LL::Debug, "EOS_Ecom_CopyTransactionByIndex (%p, %p(%d, %p, %d))", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->TransactionIndex);
	else
		log(LL::Debug, "EOS_Ecom_CopyTransactionByIndex (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_COPYTRANSACTIONBYINDEX_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_EResult::EOS_InvalidParameters;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_CopyTransactionByIndex);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options, OutTransaction);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!OutTransaction)
		return EOS_EResult::EOS_InvalidParameters;

	*OutTransaction = nullptr;

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	// TODOSTUB
	return EOS_EResult::EOS_NotFound;
}

/**
 * Fetches the transaction handle at the given index.
 *
 * @param Options structure containing the account id and transaction id being accessed
 *
 * @see EOS_Ecom_CheckoutCallbackInfo
 * @see EOS_Ecom_Transaction_Release
 *
 * @return EOS_Success if the information is available and passed out in OutTransaction
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_NotFound if the transaction is not found
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Ecom_CopyTransactionById(EOS_HEcom Handle, const EOS_Ecom_CopyTransactionByIdOptions* Options, EOS_Ecom_HTransaction* OutTransaction)
{
	if (Options && Options->TransactionId)
		log(LL::Debug, "EOS_Ecom_CopyTransactionById (%p, %p(%d, %p, %p(%s)))", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->TransactionId, Options->TransactionId);
	else
		log(LL::Debug, "EOS_Ecom_CopyTransactionById (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_COPYTRANSACTIONBYID_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_EResult::EOS_InvalidParameters;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_CopyTransactionById);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options, OutTransaction);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!OutTransaction)
		return EOS_EResult::EOS_InvalidParameters;

	*OutTransaction = nullptr;

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	// TODOSTUB
	return EOS_EResult::EOS_NotFound;
}

/**
 * The Ecom Transaction Interface exposes getters for accessing information about a completed transaction.
 * All Ecom Transaction Interface calls take a handle of type EOS_Ecom_HTransaction as the first parameter.
 * A EOS_Ecom_HTransaction handle is originally returned as part of the EOS_Ecom_CheckoutCallbackInfo struct.
 * A EOS_Ecom_HTransaction handle can also be retrieved from a EOS_HEcom handle using EOS_Ecom_GetTransactionByIndex.
 * It is expected that after a transaction that EOS_Ecom_Transaction_Release is called.
 * When EOS_Platform_Release is called any remaining transactions will also be released.
 *
 * @see EOS_Ecom_CheckoutCallbackInfo
 * @see EOS_Ecom_GetTransactionCount
 * @see EOS_Ecom_GetTransactionByIndex
 */

EOS_DECLARE_FUNC(EOS_EResult) EOS_Ecom_Transaction_GetTransactionId(EOS_Ecom_HTransaction Handle, char* OutBuffer, int32_t* InOutBufferLength)
{
	log(LL::Debug, "EOS_Ecom_Transaction_GetTransactionId (%p, %p, %p(%d))", Handle, OutBuffer, InOutBufferLength, *InOutBufferLength);

	PROXY_FUNC(EOS_Ecom_Transaction_GetTransactionId);
	if (proxied)
	{
		auto res = proxied(Handle, OutBuffer, InOutBufferLength);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	*InOutBufferLength = 0;

	EOS_CHECK_CONFIGURED();

	if (!OutBuffer)
		return EOS_EResult::EOS_InvalidParameters;

	// TODOSTUB
	return EOS_EResult::EOS_InvalidParameters;
}

/**
 * Fetch the number of entitlements that are part of this transaction.
 *
 * @param Options structure containing the account id being accessed
 *
 * @see EOS_Ecom_Transaction_CopyEntitlementByIndex
 *
 * @return the number of entitlements found.
 */
EOS_DECLARE_FUNC(uint32_t) EOS_Ecom_Transaction_GetEntitlementsCount(EOS_Ecom_HTransaction Handle, const EOS_Ecom_Transaction_GetEntitlementsCountOptions* Options)
{
	if(Options)
		log(LL::Debug, "EOS_Ecom_Transaction_GetEntitlementsCount (%p, %p(%d))", Handle, Options, Options->ApiVersion);
	else
		log(LL::Debug, "EOS_Ecom_Transaction_GetEntitlementsCount (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_TRANSACTION_GETENTITLEMENTSCOUNT_API_LATEST);

	PROXY_FUNC(EOS_Ecom_Transaction_GetEntitlementsCount);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	// TODOSTUB
	return 0;
}

/**
 * Fetches an entitlement from a given index.
 *
 * @param Options structure containing the index being accessed
 * @param OutEntitlement the entitlement for the given index, if it exists and is valid, use EOS_Ecom_Entitlement_Release when finished
 *
 * @see EOS_Ecom_Entitlement_Release
 *
 * @return EOS_Success if the information is available and passed out in OutEntitlement
 *         EOS_Ecom_EntitlementStale if the entitlement information is stale and passed out in OutEntitlement
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_NotFound if the entitlement is not found
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Ecom_Transaction_CopyEntitlementByIndex(EOS_Ecom_HTransaction Handle, const EOS_Ecom_Transaction_CopyEntitlementByIndexOptions* Options, EOS_Ecom_Entitlement ** OutEntitlement)
{
	if (Options)
		log(LL::Debug, "EOS_Ecom_Transaction_CopyEntitlementByIndex (%p, %p(%d, %d))", Handle, Options, Options->ApiVersion, Options->EntitlementIndex);
	else
		log(LL::Debug, "EOS_Ecom_Transaction_CopyEntitlementByIndex (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_TRANSACTION_COPYENTITLEMENTBYINDEX_API_LATEST);

	PROXY_FUNC(EOS_Ecom_Transaction_CopyEntitlementByIndex);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutEntitlement);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!OutEntitlement)
		return EOS_EResult::EOS_InvalidParameters;

	*OutEntitlement = nullptr;

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	// TODOSTUB
	return EOS_EResult::EOS_NotFound;
}
