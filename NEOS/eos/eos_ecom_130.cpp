#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * Fetch the number of images that are associated with a given cached offer for a local user.
 *
 * @return the number of images found.
 */
EOS_DECLARE_FUNC(uint32_t) EOS_Ecom_GetOfferImageInfoCount(EOS_HEcom Handle, const EOS_Ecom_GetOfferImageInfoCountOptions* Options)
{
	if (Options && Options->OfferId)
		log(LL::Debug, __FUNCTION__ " (%p, %p(%d, %p, %p(%s)))", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->OfferId, Options->OfferId);
	else
		log(LL::Debug, __FUNCTION__ " (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_GETOFFERIMAGEINFOCOUNT_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return 0;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_GetOfferImageInfoCount);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	// TODO?
	return 0;
}

/**
 * Fetches an image from a given index.
 *
 * @param Options structure containing the offer id and index being accessed
 * @param OutImageInfo the image for the given index, if it exists and is valid, use EOS_Ecom_KeyImageInfo_Release when finished
 *
 * @see EOS_Ecom_KeyImageInfo_Release
 *
 * @return EOS_Success if the information is available and passed out in OutImageInfo
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_Ecom_CatalogOfferStale if the associated offer information is stale
 *         EOS_NotFound if the image is not found
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Ecom_CopyOfferImageInfoByIndex(EOS_HEcom Handle, const EOS_Ecom_CopyOfferImageInfoByIndexOptions* Options, EOS_Ecom_KeyImageInfo** OutImageInfo)
{
	if (Options && Options->OfferId)
		log(LL::Debug, __FUNCTION__ " (%p, %p(%d, %p, %p(%s), %d))", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->OfferId, Options->OfferId, Options->ImageInfoIndex);
	else
		log(LL::Debug, __FUNCTION__ " (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_ECOM_COPYOFFERITEMBYINDEX_API_LATEST);

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_EResult::EOS_InvalidParameters;
	}

	auto* ecom = reinterpret_cast<HEcom*>(Handle);
	PROXY_FUNC(EOS_Ecom_CopyOfferImageInfoByIndex);
	if (proxied)
	{
		auto res = proxied(ecom->GetProxyHandle(), Options, OutImageInfo);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!OutImageInfo)
		return EOS_EResult::EOS_InvalidParameters;

	*OutImageInfo = nullptr;

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	// TODOSTUB
	return EOS_EResult::EOS_NotFound;
}
