#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * Set the callback function to use for SDK log messages. Any previously set callback will no longer be called.
 *
 * @param Callback the function to call when the SDK logs messages
 * @return EOS_Success is returned if the callback will be used for future log messages.
 *         EOS_NotConfigured is returned if the SDK has not yet been initialized, or if it has been shut down
 *
 * @see EOS_Init
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Logging_SetCallback(EOS_LogMessageFunc Callback)
{
	log(LL::Debug, "EOS_Logging_SetCallback (%p)", Callback); // TODO: we should probably make use of this

	PROXY_FUNC(EOS_Logging_SetCallback);
	if (proxied)
	{
		if (!NEOS_IsProxyLogged) // only set log callback if we aren't logging it ourselves
		{
			auto res = proxied(Callback);
			log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
			return res;
		}
	}

	EOS_CHECK_CONFIGURED();

	return EOS_EResult::EOS_Success;
}

/**
 * Set the logging level for the specified logging category. By default all log categories will callback for Warnings, Errors, and Fatals.
 *
 * @param LogCategory the specific log category to configure. Use EOS_LC_ALL_CATEGORIES to configure all categories simultaneously to the same log level.
 * @param LogLevel the log level to use for the log category
 *
 * @return EOS_Success is returned if the log levels are now in use.
 *         EOS_NotConfigured is returned if the SDK has not yet been initialized, or if it has been shut down.
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Logging_SetLogLevel(EOS_ELogCategory LogCategory, EOS_ELogLevel LogLevel)
{
	log(LL::Debug, "EOS_Logging_SetLogLevel (%x, %d)", LogCategory, LogLevel);

	PROXY_FUNC(EOS_Logging_SetLogLevel);
	if (proxied)
	{
		if (!NEOS_IsProxyLogged) // onlly set log level if we haven't set it ourselves
		{
			auto res = proxied(LogCategory, LogLevel);
			log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
			return res;
		}
	}

	EOS_CHECK_CONFIGURED();

	return EOS_EResult::EOS_Success;
}
