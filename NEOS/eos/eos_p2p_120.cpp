#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * P2P functions to help manage sending and receiving of messages to peers
 *
 * This class will attempt to perform NAT-punching via STUN, but does not currently support relays.
 */

/**
 * Send a packet to a peer at the specified address. If there is already an open connection to this peer, it will be
 * sent immediately. If there is no open connection, an attempt to connect to the peer will be made. A EOS_Success
 * result does not guarantee the packet will be delivered to the peer, as data is sent unreliably.
 *
 * @param Options Information about the data being sent, by who, to who
 * @return EOS_EResult::EOS_Success           - If packet was queued to be sent successfully
 *         EOS_EResult::EOS_InvalidParameters - If input was invalid
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_P2P_SendPacket(EOS_HP2P Handle, const EOS_P2P_SendPacketOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_P2P_SendPacket (%p(%d ...))", Options, Options->ApiVersion); // todo: print other info?
	else
		log(LL::Debug, "EOS_P2P_SendPacket (%p)", Options);

	EOS_CHECK_VERSION(EOS_P2P_SENDPACKET_API_LATEST);

	PROXY_FUNC(EOS_P2P_SendPacket);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_InvalidParameters;
}

/**
 * Gets the size of the packet that will be returned by ReceivePacket for a particular user, if there is any available
 * packets to be retrieved.
 *
 * @param Options Information about who is requesting the size of their next packet
 * @param OutPacketSize The amount of bytes required to store the data of the next packet for the requested user
 * @return EOS_EResult::EOS_Success - If OutPacketSize was successfully set and there is data to be received
 *         EOS_EResult::EOS_InvalidParameters - If input was invalid
 *         EOS_EResult::EOS_NotFound  - If there are no packets available for the requesting user
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_P2P_GetNextReceivedPacketSize(EOS_HP2P Handle, const EOS_P2P_GetNextReceivedPacketSizeOptions* Options, uint32_t* OutPacketSizeBytes)
{
	if (Options)
		log(LL::Debug, "EOS_P2P_GetNextReceivedPacketSize (%p(%d, %p, %p), %p)", Options, Options->ApiVersion, Options->LocalUserId, Options->RequestedChannel, OutPacketSizeBytes);
	else
		log(LL::Debug, "EOS_P2P_GetNextReceivedPacketSize (%p, %p)", Options, OutPacketSizeBytes);

	EOS_CHECK_VERSION(EOS_P2P_GETNEXTRECEIVEDPACKETSIZE_API_LATEST);

	PROXY_FUNC(EOS_P2P_GetNextReceivedPacketSize);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutPacketSizeBytes);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!OutPacketSizeBytes)
		return EOS_EResult::EOS_InvalidParameters;

	*OutPacketSizeBytes = 0;

	if (!Options)
		return EOS_EResult::EOS_InvalidParameters;

	// TODOSTUB
	return EOS_EResult::EOS_NotFound;
}

/**
 * Receive the next packet for the local user, and information associated with this packet, if it exists.
 *
 * @param Options Information about who is requesting the size of their next packet, and how much data can be stored safely
 * @param OutPeerId The Remote User who sent data. Only set if there was a packet to receive.
 * @param OutSocketId The Socket ID of the data that was sent. Only set if there was a packet to receive.
 * @param OutChannel The channel the data was sent on. Only set if there was a packet to receive.
 * @param OutData Buffer to store the data being received. Must be at least EOS_P2P_GetNextReceivedPacketSize in length or data will be truncated
 * @param OutBytesWritten The amount of bytes written to OutData. Only set if there was a packet to receive.
 * @return EOS_EResult::EOS_Success - If the packet was received successfully
 *         EOS_EResult::EOS_InvalidParameters - If input was invalid
 *         EOS_EREsult::EOS_NotFound - If there are no packets available for the requesting user
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_P2P_ReceivePacket(EOS_HP2P Handle, const EOS_P2P_ReceivePacketOptions* Options, EOS_ProductUserId* OutPeerId, EOS_P2P_SocketId* OutSocketId, uint8_t* OutChannel, void* OutData, uint32_t* OutBytesWritten)
{
	if (Options)
		log(LL::Debug, "EOS_P2P_ReceivePacket (%p(%d, %p, %d, %p), %p, %p, %p, %p, %p)", Options, Options->ApiVersion, Options->LocalUserId, Options->MaxDataSizeBytes, Options->RequestedChannel, OutPeerId, OutSocketId, OutChannel, OutData, OutBytesWritten);
	else
		log(LL::Debug, "EOS_P2P_ReceivePacket (%p, %p, %p, %p, %p, %p)", Options, OutPeerId, OutSocketId, OutChannel, OutData, OutBytesWritten);

	EOS_CHECK_VERSION(EOS_P2P_RECEIVEPACKET_API_LATEST);

	PROXY_FUNC(EOS_P2P_ReceivePacket);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutPeerId, OutSocketId, OutChannel, OutData, OutBytesWritten);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!OutBytesWritten)
		return EOS_EResult::EOS_InvalidParameters;

	*OutBytesWritten = 0;

	if (!Options || !OutPeerId || !OutSocketId || !OutChannel || !OutData)
		return EOS_EResult::EOS_InvalidParameters;

	// TODOSTUB
	return EOS_EResult::EOS_NotFound;
}

/**
 * Listen for incoming connection requests on a particular Socket ID, or optionally all Socket IDs. The bound function
 * will only be called if the connection has not already been accepted.
 *
 * @param Options Information about who would like notifications, and (optionally) only for a specific socket
 * @param ClientData This value is returned to the caller when ConnectionRequestHandler is invoked
 * @param ConnectionRequestHandler The callback to be fired when we receive a connection request
 * @return A valid notification ID if successfully bound, or EOS_INVALID_NOTIFICATIONID otherwise
 */
EOS_DECLARE_FUNC(EOS_NotificationId) EOS_P2P_AddNotifyPeerConnectionRequest(EOS_HP2P Handle, const EOS_P2P_AddNotifyPeerConnectionRequestOptions* Options, void* ClientData, EOS_P2P_OnIncomingConnectionRequestCallback ConnectionRequestHandler)
{
	if (Options)
		log(LL::Debug, "EOS_P2P_AddNotifyPeerConnectionRequest (%p(%d, %p, %p), %p, %p)", Options, Options->ApiVersion, Options->LocalUserId, Options->SocketId, ClientData, ConnectionRequestHandler);
	else
		log(LL::Debug, "EOS_P2P_AddNotifyPeerConnectionRequest (%p, %p, %p)", Options, ClientData, ConnectionRequestHandler);

	EOS_CHECK_VERSION(EOS_P2P_ADDNOTIFYPEERCONNECTIONREQUEST_API_LATEST);

	PROXY_FUNC(EOS_P2P_AddNotifyPeerConnectionRequest);
	if (proxied)
	{
		auto res = proxied(Handle, Options, ClientData, ConnectionRequestHandler);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	// TODOSTUB
	return EOS_INVALID_NOTIFICATIONID;
}

/**
 * Stop listening for connection requests on a previously bound handler
 *
 * @param NotificationId The previously bound notification ID
 */
EOS_DECLARE_FUNC(void) EOS_P2P_RemoveNotifyPeerConnectionRequest(EOS_HP2P Handle, EOS_NotificationId NotificationId)
{
	log(LL::Debug, "EOS_P2P_RemoveNotifyPeerConnectionRequest (%d)", NotificationId);

	PROXY_FUNC(EOS_P2P_RemoveNotifyPeerConnectionRequest);
	if (proxied)
	{
		proxied(Handle, NotificationId);
		return;
	}

	// TODOSTUB
	return;
}

/**
 * Listen for when a previously opened connection is closed
 *
 * @param Options Information about who would like notifications about closed connections, and for which socket
 * @param ClientData This value is returned to the caller when ConnectionClosedHandler is invoked
 * @param ConnectionClosedHandler The callback to be fired when we an open connection has been closed
 * @return A valid notification ID if successfully bound, or EOS_INVALID_NOTIFICATIONID otherwise
 */
EOS_DECLARE_FUNC(EOS_NotificationId) EOS_P2P_AddNotifyPeerConnectionClosed(EOS_HP2P Handle, const EOS_P2P_AddNotifyPeerConnectionClosedOptions* Options, void* ClientData, EOS_P2P_OnRemoteConnectionClosedCallback ConnectionClosedHandler)
{
	if (Options)
		log(LL::Debug, "EOS_P2P_AddNotifyPeerConnectionClosed (%p(%d, %p, %p), %p, %p)", Options, Options->ApiVersion, Options->LocalUserId, Options->SocketId, ClientData, ConnectionClosedHandler);
	else
		log(LL::Debug, "EOS_P2P_AddNotifyPeerConnectionClosed (%p, %p, %p)", Options, ClientData, ConnectionClosedHandler);

	EOS_CHECK_VERSION(EOS_P2P_ADDNOTIFYPEERCONNECTIONCLOSED_API_LATEST);

	PROXY_FUNC(EOS_P2P_AddNotifyPeerConnectionClosed);
	if (proxied)
	{
		auto res = proxied(Handle, Options, ClientData, ConnectionClosedHandler);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	// TODOSTUB
	return EOS_INVALID_NOTIFICATIONID;
}

/**
 * Stop notifications for connections being closed on a previously bound handler
 *
 * @param NotificationId The previously bound notification ID
 */
EOS_DECLARE_FUNC(void) EOS_P2P_RemoveNotifyPeerConnectionClosed(EOS_HP2P Handle, EOS_NotificationId NotificationId)
{
	log(LL::Debug, "EOS_P2P_RemoveNotifyPeerConnectionClosed (%d)", NotificationId);

	PROXY_FUNC(EOS_P2P_RemoveNotifyPeerConnectionClosed);
	if (proxied)
	{
		proxied(Handle, NotificationId);
		return;
	}

	// TODOSTUB
	return;
}

/**
 * Accept connections from a specific peer. If this peer has not attempted to connect yet, when they do, they will automatically be accepted.
 *
 * @param Options Information about who would like to accept a connection, and which connection
 * @return EOS_EResult::EOS_Success - if the provided data is valid
 *         EOS_EREsult::EOS_InvalidParameters - if the provided data is invalid
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_P2P_AcceptConnection(EOS_HP2P Handle, const EOS_P2P_AcceptConnectionOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_P2P_AcceptConnection (%p(%d, %p, %p, %p))", Options, Options->ApiVersion, Options->LocalUserId, Options->RemoteUserId, Options->SocketId);
	else
		log(LL::Debug, "EOS_P2P_AcceptConnection (%p)", Options);

	EOS_CHECK_VERSION(EOS_P2P_ACCEPTCONNECTION_API_LATEST);

	PROXY_FUNC(EOS_P2P_AcceptConnection);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * Stop accepting new connections from a specific peer and close any open connections.
 *
 * @param Options Information about who would like to close a connection, and which connection.
 * @return EOS_EResult::EOS_Success - if the provided data is valid
 *         EOS_EREsult::EOS_InvalidParameters - if the provided data is invalid
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_P2P_CloseConnection(EOS_HP2P Handle, const EOS_P2P_CloseConnectionOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_P2P_CloseConnection (%p(%d, %p, %p, %p))", Options, Options->ApiVersion, Options->LocalUserId, Options->RemoteUserId, Options->SocketId);
	else
		log(LL::Debug, "EOS_P2P_CloseConnection (%p)", Options);

	EOS_CHECK_VERSION(EOS_P2P_CLOSECONNECTION_API_LATEST);

	PROXY_FUNC(EOS_P2P_CloseConnection);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * Close any open Connections for a specific Peer Connection ID.
 *
 * @param Options Information about who would like to close connections, and by what socket ID
 * @return EOS_EResult::EOS_Success - if the provided data is valid
 *         EOS_EREsult::EOS_InvalidParameters - if the provided data is invalid
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_P2P_CloseConnections(EOS_HP2P Handle, const EOS_P2P_CloseConnectionsOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_P2P_CloseConnections (%p(%d, %p, %p))", Options, Options->ApiVersion, Options->LocalUserId, Options->SocketId);
	else
		log(LL::Debug, "EOS_P2P_CloseConnections (%p)", Options);

	EOS_CHECK_VERSION(EOS_P2P_CLOSECONNECTIONS_API_LATEST);

	PROXY_FUNC(EOS_P2P_CloseConnections);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * Query the current NAT-type of our connection.
 *
 * @param Options Information about what version of the EOS_P2P_QueryNATType API they support
 * @param NATTypeQueriedHandler The callback to be fired when we finish querying our NAT type
 */
EOS_DECLARE_FUNC(void) EOS_P2P_QueryNATType(EOS_HP2P Handle, const EOS_P2P_QueryNATTypeOptions* Options, void* ClientData, const EOS_P2P_OnQueryNATTypeCompleteCallback NATTypeQueriedHandler)
{
	if (Options)
		log(LL::Debug, "EOS_P2P_QueryNATType (%p(%d), %p, %p)", Options, Options->ApiVersion, ClientData, NATTypeQueriedHandler);
	else
		log(LL::Debug, "EOS_P2P_QueryNATType (%p, %p, %p)", Options, ClientData, NATTypeQueriedHandler);

	EOS_CHECK_VERSION(EOS_P2P_QUERYNATTYPE_API_LATEST);

	PROXY_FUNC(EOS_P2P_QueryNATType);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, NATTypeQueriedHandler);
		return;
	}

	// TODOASYNC
	return;
}

/**
 * Get our last-queried NAT-type, if it has been successfully queried.
 *
 * @param Options Information about what version of the EOS_P2P_GetNATType API they support
 * @param OutNATType The queried NAT Type, or unknown if unknown
 * @return EOS_EResult::EOS_Success - if we have cached data
 *         EOS_EResult::EOS_NotFound - If we do not have queried data cached
 *         EOS_EResult::EOS_IncompatibleVersion - If the provided version is unknown
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_P2P_GetNATType(EOS_HP2P Handle, const EOS_P2P_GetNATTypeOptions* Options, EOS_ENATType* OutNATType)
{
	if (Options)
		log(LL::Debug, "EOS_P2P_GetNATType (%p(%d), %p)", Options, Options->ApiVersion, OutNATType);
	else
		log(LL::Debug, "EOS_P2P_GetNATType (%p, %p)", Options, OutNATType);

	EOS_CHECK_VERSION(EOS_P2P_GETNATTYPE_API_LATEST);

	PROXY_FUNC(EOS_P2P_GetNATType);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutNATType);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_InvalidParameters;
}
