#include "../stdafx.h"
#include "../NEOS.hpp"

// ugh - can't find a good way to get rid of this warning...
#pragma warning( push )
#pragma warning( disable : 4312 )
/**
 * Get a handle to the Connect Interface.
 * @return EOS_HConnect handle
 *
 * @see eos_connect.h
 * @see eos_connect_types.h
 */
EOS_DECLARE_FUNC(EOS_HConnect) EOS_Platform_GetConnectInterface(EOS_HPlatform Handle)
{
	log(LL::Debug, "EOS_Platform_GetConnectInterface (%p)", Handle);

	if (!Handle)
		return nullptr; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_GetConnectInterface);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle());
		log(LL::Debug, " + Proxy Result: %p", res);
		return res;
	}

	return reinterpret_cast<EOS_HConnect>(platform->GetHConnect());
}

/**
 * Get a handle to the Sessions Interface.
 * @return EOS_HSessions handle
 *
 * @see eos_sessions.h
 * @see eos_sessions_types.h
 */
EOS_DECLARE_FUNC(EOS_HSessions) EOS_Platform_GetSessionsInterface(EOS_HPlatform Handle)
{
	log(LL::Debug, "EOS_Platform_GetSessionsInterface (%p)", Handle);

	if (!Handle)
		return nullptr; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_GetSessionsInterface);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle());
		log(LL::Debug, " + Proxy Result: %p", res);
		return res;
	}

	return reinterpret_cast<EOS_HSessions>(NEOS_HSESSIONS);
}

/**
 * Get a handle to the Peer-to-Peer Networking Interface.
 * @return EOS_HP2P handle
 *
 * @see eos_p2p.h
 * @see eos_p2p_types.h
 */
EOS_DECLARE_FUNC(EOS_HP2P) EOS_Platform_GetP2PInterface(EOS_HPlatform Handle)
{
	log(LL::Debug, "EOS_Platform_GetP2PInterface (%p)", Handle);

	if (!Handle)
		return nullptr; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_GetP2PInterface);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle());
		log(LL::Debug, " + Proxy Result: %p", res);
		return res;
	}

	return reinterpret_cast<EOS_HP2P>(NEOS_HP2P);
}
#pragma warning( pop )

/**
 * Get the active country code that the SDK will send to services which require it.
 * This returns the override value otherwise it will use the country code of the given user.
 * This is currently used for determining pricing.
 *
 * @param LocalUserId The account to use for lookup if no override exists.
 * @param OutBuffer The buffer into which the character data should be written.  The buffer must be long enough to hold a string of EOS_COUNTRYCODE_MAX_LEN.
 * @param InOutBufferLength The size of the OutBuffer in characters.
 *                          The input buffer should include enough space to be null-terminated.
 *                          When the function returns, this parameter will be filled with the length of the string copied into OutBuffer.
 *
 * @return An EOS_EResult that indicates whether the active country code string was copied into the OutBuffer.
 *         EOS_Success if the information is available and passed out in OutBuffer
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_NotFound if there is neither an override nor an available country code for the user.
 *         EOS_LimitExceeded - The OutBuffer is not large enough to receive the country code string. InOutBufferLength contains the required minimum length to perform the operation successfully.
 *
 * @see eos_ecom.h
 * @see EOS_COUNTRYCODE_MAX_LEN
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Platform_GetActiveCountryCode(EOS_HPlatform Handle, EOS_EpicAccountId LocalUserId, char* OutBuffer, int32_t* InOutBufferLength)
{
	log(LL::Debug, "EOS_Platform_GetActiveCountryCode (%p, %p, %p, %p(%d))", Handle, LocalUserId, OutBuffer, InOutBufferLength, *InOutBufferLength);

	EOS_CHECK_CONFIGURED();

	if (!Handle)
		return EOS_EResult::EOS_InvalidParameters; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_GetActiveCountryCode);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle(), LocalUserId, OutBuffer, InOutBufferLength);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	// TODO: check LocalUserId?
	return EOS_Platform_GetOverrideCountryCode(Handle, OutBuffer, InOutBufferLength);
}

/**
 * Get the active locale code that the SDK will send to services which require it.
 * This returns the override value otherwise it will use the locale code of the given user.
 * This is used for localization. This follows ISO 639.
 *
 * @param LocalUserId The account to use for lookup if no override exists.
 * @param OutBuffer The buffer into which the character data should be written.  The buffer must be long enough to hold a string of EOS_LOCALECODE_MAX_LEN.
 * @param InOutBufferLength The size of the OutBuffer in characters.
 *                          The input buffer should include enough space to be null-terminated.
 *                          When the function returns, this parameter will be filled with the length of the string copied into OutBuffer.
 *
 * @return An EOS_EResult that indicates whether the active locale code string was copied into the OutBuffer.
 *         EOS_Success if the information is available and passed out in OutBuffer
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_NotFound if there is neither an override nor an available locale code for the user.
 *         EOS_LimitExceeded - The OutBuffer is not large enough to receive the locale code string. InOutBufferLength contains the required minimum length to perform the operation successfully.
 *
 * @see eos_ecom.h
 * @see EOS_LOCALECODE_MAX_LEN
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Platform_GetActiveLocaleCode(EOS_HPlatform Handle, EOS_EpicAccountId LocalUserId, char* OutBuffer, int32_t* InOutBufferLength)
{
	log(LL::Debug, "EOS_Platform_GetActiveLocaleCode (%p, %p, %p, %p(%d))", Handle, LocalUserId, OutBuffer, InOutBufferLength, *InOutBufferLength);

	EOS_CHECK_CONFIGURED();

	if (!Handle)
		return EOS_EResult::EOS_InvalidParameters; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_GetActiveLocaleCode);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle(), LocalUserId, OutBuffer, InOutBufferLength);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	// TODO: check LocalUserId?
	return EOS_Platform_GetOverrideLocaleCode(Handle, OutBuffer, InOutBufferLength);
}

/**
 * Get the override country code that the SDK will send to services which require it.
 * This is currently used for determining pricing.
 *
 * @param OutBuffer The buffer into which the character data should be written.  The buffer must be long enough to hold a string of EOS_COUNTRYCODE_MAX_LEN.
 * @param InOutBufferLength The size of the OutBuffer in characters.
 *                          The input buffer should include enough space to be null-terminated.
 *                          When the function returns, this parameter will be filled with the length of the string copied into OutBuffer.
 *
 * @return An EOS_EResult that indicates whether the override country code string was copied into the OutBuffer.
 *         EOS_Success if the information is available and passed out in OutBuffer
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_LimitExceeded - The OutBuffer is not large enough to receive the country code string. InOutBufferLength contains the required minimum length to perform the operation successfully.
 *
 * @see eos_ecom.h
 * @see EOS_COUNTRYCODE_MAX_LEN
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Platform_GetOverrideCountryCode(EOS_HPlatform Handle, char* OutBuffer, int32_t* InOutBufferLength)
{
	log(LL::Debug, "EOS_Platform_GetOverrideCountryCode (%p, %p, %p(%d))", Handle, OutBuffer, InOutBufferLength, *InOutBufferLength);

	EOS_CHECK_CONFIGURED();

	if (!Handle)
		return EOS_EResult::EOS_InvalidParameters; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_GetOverrideCountryCode);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle(), OutBuffer, InOutBufferLength);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	if (!InOutBufferLength)
		return EOS_EResult::EOS_InvalidParameters;

	int32_t inputLength = *InOutBufferLength;
	*InOutBufferLength = static_cast<int32_t>(UserCountry.length()) + 1; // TODO: should we do this?

	if (!OutBuffer)
		return EOS_EResult::EOS_InvalidParameters;

	if (UserCountry.length() + 1 > inputLength)
		return EOS_EResult::EOS_LimitExceeded;

	memcpy(OutBuffer, UserCountry.c_str(), UserCountry.length());
	OutBuffer[UserCountry.length()] = 0;
	return EOS_EResult::EOS_Success;
}

/**
 * Get the override locale code that the SDK will send to services which require it.
 * This is used for localization. This follows ISO 639.
 *
 * @param OutBuffer The buffer into which the character data should be written.  The buffer must be long enough to hold a string of EOS_LOCALECODE_MAX_LEN.
 * @param InOutBufferLength The size of the OutBuffer in characters.
 *                          The input buffer should include enough space to be null-terminated.
 *                          When the function returns, this parameter will be filled with the length of the string copied into OutBuffer.
 *
 * @return An EOS_EResult that indicates whether the override locale code string was copied into the OutBuffer.
 *         EOS_Success if the information is available and passed out in OutBuffer
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_LimitExceeded - The OutBuffer is not large enough to receive the locale code string. InOutBufferLength contains the required minimum length to perform the operation successfully.
 *
 * @see eos_ecom.h
 * @see EOS_LOCALECODE_MAX_LEN
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Platform_GetOverrideLocaleCode(EOS_HPlatform Handle, char* OutBuffer, int32_t* InOutBufferLength)
{
	log(LL::Debug, "EOS_Platform_GetOverrideLocaleCode (%p, %p, %p(%d))", Handle, OutBuffer, InOutBufferLength, *InOutBufferLength);

	EOS_CHECK_CONFIGURED();

	if (!Handle)
		return EOS_EResult::EOS_InvalidParameters; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_GetOverrideLocaleCode);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle(), OutBuffer, InOutBufferLength);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	if(!InOutBufferLength)
		return EOS_EResult::EOS_InvalidParameters;

	int32_t inputLength = *InOutBufferLength;
	*InOutBufferLength = static_cast<int32_t>(UserLanguage.length()) + 1; // TODO: should we do this?

	if (!OutBuffer)
		return EOS_EResult::EOS_InvalidParameters;

	if (UserLanguage.length() + 1 > inputLength)
		return EOS_EResult::EOS_LimitExceeded;

	memcpy(OutBuffer, UserLanguage.c_str(), UserLanguage.length());
	OutBuffer[UserLanguage.length()] = 0;
	return EOS_EResult::EOS_Success;
}

/**
 * Set the override country code that the SDK will send to services which require it.
 * This is currently used for determining accurate pricing.
 *
 * @return An EOS_EResult that indicates whether the override country code string was saved.
 *         EOS_Success if the country code was overridden
 *         EOS_InvalidParameters if you pass an invalid country code
 *
 * @see eos_ecom.h
 * @see EOS_COUNTRYCODE_MAX_LEN
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Platform_SetOverrideCountryCode(EOS_HPlatform Handle, const char* NewCountryCode)
{
	log(LL::Info, "EOS_Platform_SetOverrideCountryCode (%p, %p(%s))", Handle, NewCountryCode, NewCountryCode);

	EOS_CHECK_CONFIGURED();

	if (!Handle)
		return EOS_EResult::EOS_InvalidParameters; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_SetOverrideCountryCode);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle(), NewCountryCode);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	// todo: validate NewCountryCode!
	UserCountry = NewCountryCode; // todo: HPlatform::m_CountryOverride ?
	return EOS_EResult::EOS_Success;
}

/**
 * Set the override locale code that the SDK will send to services which require it.
 * This is used for localization. This follows ISO 639.
 *
 * @return An EOS_EResult that indicates whether the override locale code string was saved.
 *         EOS_Success if the locale code was overridden
 *         EOS_InvalidParameters if you pass an invalid locale code
 *
 * @see eos_ecom.h
 * @see EOS_LOCALECODE_MAX_LEN
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Platform_SetOverrideLocaleCode(EOS_HPlatform Handle, const char* NewLocaleCode)
{
	log(LL::Info, "EOS_Platform_SetOverrideLocaleCode (%p, %p(%s))", Handle, NewLocaleCode, NewLocaleCode);

	EOS_CHECK_CONFIGURED();

	if (!Handle)
		return EOS_EResult::EOS_InvalidParameters; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_SetOverrideLocaleCode);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle(), NewLocaleCode);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	// todo: validate NewLocaleCode!
	UserLanguage = NewLocaleCode; // todo: HPlatform::m_LocaleOverride ?
	return EOS_EResult::EOS_Success;
}

/**
 * Checks if the app was launched through the Epic Launcher, and relaunches it through the Epic Launcher if it wasn't.
 *
 * @return An EOS_EResult is returned to indicate success or an error.
 *
 * EOS_Success is returned if the app is being restarted. You should quit your process as soon as possible.
 * EOS_NoChange is returned if the app was already launched through the Epic Launcher, and no action needs to be taken.
 * EOS_UnexpectedError is returned if the LauncherCheck module failed to initialize, or the module tried and failed to restart the app.
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Platform_CheckForLauncherAndRestart(EOS_HPlatform Handle)
{
	log(LL::Debug, "EOS_Platform_CheckForLauncherAndRestart (%p)", Handle);

	EOS_CHECK_CONFIGURED();

	if (!Handle)
		return EOS_EResult::EOS_InvalidParameters; // shouldn't ever happen, hopefully

	auto* platform = reinterpret_cast<HPlatform*>(Handle);

	PROXY_FUNC(EOS_Platform_CheckForLauncherAndRestart);
	if (proxied)
	{
		auto res = proxied(platform->GetProxyHandle());
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	return NEOS_RestartAppIfNecessary() ? EOS_EResult::EOS_Success : EOS_EResult::EOS_NoChange;
}
