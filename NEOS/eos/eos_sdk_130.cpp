#include "../stdafx.h"
#include "../NEOS.hpp"

// ugh - can't find a good way to get rid of this warning...
#pragma warning( push )
#pragma warning( disable : 4312 )
/**
 * Get a handle to the PlayerDataStorage Interface.
 * @return EOS_HPlayerDataStorage handle
 *
 * @see eos_playerdatastorage.h
 * @see eos_playerdatastorage_types.h
 */
EOS_DECLARE_FUNC(EOS_HPlayerDataStorage) EOS_Platform_GetPlayerDataStorageInterface(EOS_HPlatform Handle)
{
  log(LL::Debug, __FUNCTION__ " (%p)", Handle);

  if (!Handle)
    return nullptr; // shouldn't ever happen, hopefully

  auto* platform = reinterpret_cast<HPlatform*>(Handle);

  PROXY_FUNC(EOS_Platform_GetPlayerDataStorageInterface);
  if (proxied)
  {
    auto res = proxied(platform->GetProxyHandle());
    log(LL::Debug, " + Proxy Result: %p", res);
    return res;
  }

  return reinterpret_cast<EOS_HPlayerDataStorage>(NEOS_HPLAYERDATASTORAGE);
}

/**
 * Get a handle to the Achievements Interface.
 * @return EOS_HAchievements handle
 *
 * @see eos_achievements.h
 * @see eos_achievements_types.h
 */
EOS_DECLARE_FUNC(EOS_HAchievements) EOS_Platform_GetAchievementsInterface(EOS_HPlatform Handle)
{
  log(LL::Debug, __FUNCTION__ " (%p)", Handle);

  if (!Handle)
    return nullptr; // shouldn't ever happen, hopefully

  auto* platform = reinterpret_cast<HPlatform*>(Handle);

  PROXY_FUNC(EOS_Platform_GetAchievementsInterface);
  if (proxied)
  {
    auto res = proxied(platform->GetProxyHandle());
    log(LL::Debug, " + Proxy Result: %p", res);
    return res;
  }

  return reinterpret_cast<EOS_HAchievements>(NEOS_HACHIEVEMENTS);
}

/**
 * Get a handle to the Stats Interface.
 * @return EOS_HStats handle
 *
 * @see eos_stats.h
 * @see eos_stats_types.h
 */
EOS_DECLARE_FUNC(EOS_HStats) EOS_Platform_GetStatsInterface(EOS_HPlatform Handle)
{
  log(LL::Debug, __FUNCTION__ " (%p)", Handle);

  if (!Handle)
    return nullptr; // shouldn't ever happen, hopefully

  auto* platform = reinterpret_cast<HPlatform*>(Handle);

  PROXY_FUNC(EOS_Platform_GetStatsInterface);
  if (proxied)
  {
    auto res = proxied(platform->GetProxyHandle());
    log(LL::Debug, " + Proxy Result: %p", res);
    return res;
  }

  return reinterpret_cast<EOS_HStats>(NEOS_HSTATS);
}
#pragma warning( pop )
