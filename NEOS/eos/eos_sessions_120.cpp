#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * Release the memory associated with session modification. 
 * This must be called on data retrieved from EOS_Sessions_CreateSessionModification or EOS_Sessions_UpdateSessionModification
 *
 * @param SessionModificationHandle - The session modification handle to release
 *
 * @see EOS_Sessions_CreateSessionModification
 * @see EOS_Sessions_UpdateSessionModification
 */
EOS_DECLARE_FUNC(void) EOS_SessionModification_Release(EOS_HSessionModification SessionModificationHandle)
{
	log(LL::Debug, "EOS_SessionModification_Release (%p)", SessionModificationHandle);

	PROXY_FUNC(EOS_SessionModification_Release);
	if (proxied)
	{
		proxied(SessionModificationHandle);
		return;
	}

	// TODOSTUB
	return;
}

/**
 * Release the memory associated with an active session.
 * This must be called on data retrieved from EOS_Sessions_CopyActiveSessionHandle
 *
 * @param ActiveSessionHandle - The active session handle to release
 *
 * @see EOS_Sessions_CopyActiveSessionHandle
 */
EOS_DECLARE_FUNC(void) EOS_ActiveSession_Release(EOS_HActiveSession ActiveSessionHandle)
{
	log(LL::Debug, "EOS_ActiveSession_Release (%p)", ActiveSessionHandle);

	PROXY_FUNC(EOS_ActiveSession_Release);
	if (proxied)
	{
		proxied(ActiveSessionHandle);
		return;
	}

	// TODOSTUB
	return;
}

/**
 * Release the memory associated with a single session. This must be called on data retrieved from EOS_SessionSearch_CopySearchResultByIndex.
 *
 * @param SessionHandle - The session handle to release
 *
 * @see EOS_SessionSearch_CopySearchResultByIndex
 */
EOS_DECLARE_FUNC(void) EOS_SessionDetails_Release(EOS_HSessionDetails SessionHandle)
{
	log(LL::Debug, "EOS_SessionDetails_Release (%p)", SessionHandle);

	PROXY_FUNC(EOS_SessionDetails_Release);
	if (proxied)
	{
		proxied(SessionHandle);
		return;
	}

	// TODOSTUB
	return;
}

/**
 * Release the memory associated with a session search. This must be called on data retrieved from EOS_Sessions_CreateSessionSearch.
 *
 * @param SessionSearchHandle - The session search handle to release
 *
 * @see EOS_Sessions_CreateSessionSearch
 */
EOS_DECLARE_FUNC(void) EOS_SessionSearch_Release(EOS_HSessionSearch SessionSearchHandle)
{
	log(LL::Debug, "EOS_SessionSearch_Release (%p)", SessionSearchHandle);

	PROXY_FUNC(EOS_SessionSearch_Release);
	if (proxied)
	{
		proxied(SessionSearchHandle);
		return;
	}

	// TODOSTUB
	return;
}

/**
 * Release the memory associated with a session attribute. This must be called on data retrieved from EOS_SessionDetails_CopySessionAttributeByIndex.
 *
 * @param SessionAttribute - The session attribute to release
 *
 * @see EOS_SessionDetails_CopySessionAttributeByIndex
 */
EOS_DECLARE_FUNC(void) EOS_SessionDetails_Attribute_Release(EOS_SessionDetails_Attribute* SessionAttribute)
{
	log(LL::Debug, "EOS_SessionDetails_Attribute_Release (%p)", SessionAttribute);

	PROXY_FUNC(EOS_SessionDetails_Attribute_Release);
	if (proxied)
	{
		proxied(SessionAttribute);
		return;
	}

	// TODOSTUB
	return;
}

EOS_DECLARE_FUNC(void) EOS_SessionDetails_Info_Release(EOS_SessionDetails_Info* SessionInfo)
{
	log(LL::Debug, "EOS_SessionDetails_Info_Release (%p)", SessionInfo);

	PROXY_FUNC(EOS_SessionDetails_Info_Release);
	if (proxied)
	{
		proxied(SessionInfo);
		return;
	}

	// TODOSTUB
	return;
}

/**
 * Release the memory associated with an EOS_ActiveSession_Info structure. This must be called on data retrieved from EOS_ActiveSession_CopyInfo.
 *
 * @param ActiveSessionInfo - The active session structure to be released
 *
 * @see EOS_ActiveSession_Info
 * @see EOS_ActiveSession_CopyInfo
 */
EOS_DECLARE_FUNC(void) EOS_ActiveSession_Info_Release(EOS_ActiveSession_Info* ActiveSessionInfo)
{
	log(LL::Debug, "EOS_ActiveSession_Info_Release (%p)", ActiveSessionInfo);

	PROXY_FUNC(EOS_ActiveSession_Info_Release);
	if (proxied)
	{
		proxied(ActiveSessionInfo);
		return;
	}

	// TODOSTUB
	return;
}

/**
 * The Session Interface is used to manage sessions that can be advertised with the backend service
 * All Session Interface calls take a handle of type EOS_HSessions as the first parameter.
 * This handle can be retrieved from a EOS_HPlatform handle by using the EOS_Platform_GetSessionsInterface function.
 *
 * NOTE: At this time, this feature is only available for products that are part of the Epic Games store.
 *
 * @see EOS_Platform_GetSessionsInterface
 */

/**
 * Creates a session modification handle (EOS_HSessionModification).  The session modification handle is used to build a new session and can be applied with EOS_Sessions_UpdateSession
 * The EOS_HSessionModification must be released by calling EOS_SessionModification_Release once it no longer needed.
 *
 * @param Options Required fields for the creation of a session such as a name, bucketid, and max players
 * @param OutSessionModificationHandle Pointer to a Session Modification Handle only set if successful
 * @return EOS_Success if we successfully created the Session Modification Handle pointed at in OutSessionModificationHandle, or an error result if the input data was invalid
 *
 * @see EOS_SessionModification_Release
 * @see EOS_Sessions_UpdateSession
 * @see EOS_SessionModification_*
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Sessions_CreateSessionModification(EOS_HSessions Handle, const EOS_Sessions_CreateSessionModificationOptions* Options, EOS_HSessionModification* OutSessionModificationHandle)
{
	if (Options && Options->SessionName)
		log(LL::Debug, "EOS_Sessions_CreateSessionModification (%p(%d, %p(%s), %p(%s), %d, %p), %p)", Options, Options->ApiVersion, Options->SessionName, Options->SessionName, Options->BucketId, Options->BucketId, Options->MaxPlayers, Options->LocalUserId, OutSessionModificationHandle);
	else
		log(LL::Debug, "EOS_Sessions_CreateSessionModification (%p, %p)", Options, OutSessionModificationHandle);

	EOS_CHECK_VERSION(EOS_SESSIONS_CREATESESSIONMODIFICATION_API_LATEST);

	PROXY_FUNC(EOS_Sessions_CreateSessionModification);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutSessionModificationHandle);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_InvalidParameters;
}

/**
 * Creates a session modification handle (EOS_HSessionModification). The session modification handle is used to modify an existing session and can be applied with EOS_Sessions_UpdateSession.
 * The EOS_HSessionModification must be released by calling EOS_SessionModification_Release once it no longer needed.
 *
 * @param Options Required fields such as session name
 * @param OutSessionModificationHandle Pointer to a Session Modification Handle only set if successful
 * @return EOS_Success if we successfully created the Session Modification Handle pointed at in OutSessionModificationHandle, or an error result if the input data was invalid
 *
 * @see EOS_SessionModification_Release
 * @see EOS_Sessions_UpdateSession
 * @see EOS_SessionModification_*
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Sessions_UpdateSessionModification(EOS_HSessions Handle, const EOS_Sessions_UpdateSessionModificationOptions* Options, EOS_HSessionModification* OutSessionModificationHandle)
{
	if (Options && Options->SessionName)
		log(LL::Debug, "EOS_Sessions_UpdateSessionModification (%p(%d, %p(%s)), %p)", Options, Options->ApiVersion, Options->SessionName, Options->SessionName, OutSessionModificationHandle);
	else
		log(LL::Debug, "EOS_Sessions_UpdateSessionModification (%p, %p)", Options, OutSessionModificationHandle);

	EOS_CHECK_VERSION(EOS_SESSIONS_UPDATESESSIONMODIFICATION_API_LATEST);

	PROXY_FUNC(EOS_Sessions_UpdateSessionModification);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutSessionModificationHandle);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_InvalidParameters;
}

/**
 * Update a session given a session modification handle created via EOS_Sessions_CreateSessionModification or EOS_Sessions_UpdateSessionModification
 *
 * @param Options Structure containing information about the session to be updated
 * @param ClientData Arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate A callback that is fired when the update operation completes, either successfully or in error
 *
 * @return EOS_Success if the update completes successfully
 *         EOS_InvalidParameters if any of the options are incorrect
 *         EOS_Sessions_OutOfSync if the session is out of sync and will be updated on the next connection with the backend
 *         EOS_NotFound if a session to be updated does not exist
 */
EOS_DECLARE_FUNC(void) EOS_Sessions_UpdateSession(EOS_HSessions Handle, const EOS_Sessions_UpdateSessionOptions* Options, void* ClientData, const EOS_Sessions_OnUpdateSessionCallback CompletionDelegate)
{
	if (Options)
		log(LL::Debug, "EOS_Sessions_UpdateSession (%p(%d, %p), %p, %p)", Options, Options->ApiVersion, Options->SessionModificationHandle, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Sessions_UpdateSession (%p, %p, %p)", Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_SESSIONS_UPDATESESSION_API_LATEST);

	PROXY_FUNC(EOS_Sessions_UpdateSession);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// TODOASYNC
	return;
}

/**
 * Destroy a session given a session name
 *
 * @param Options Structure containing information about the session to be destroyed
 * @param ClientData Arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate A callback that is fired when the destroy operation completes, either successfully or in error
 *
 * @return EOS_Success if the destroy completes successfully
 *         EOS_InvalidParameters if any of the options are incorrect
 *         EOS_AlreadyPending if the session is already marked for destroy
 *         EOS_NotFound if a session to be destroyed does not exist
 */
EOS_DECLARE_FUNC(void) EOS_Sessions_DestroySession(EOS_HSessions Handle, const EOS_Sessions_DestroySessionOptions* Options, void* ClientData, const EOS_Sessions_OnDestroySessionCallback CompletionDelegate)
{
	if (Options && Options->SessionName)
		log(LL::Debug, "EOS_Sessions_DestroySession (%p(%d, %p(%s)), %p, %p)", Options, Options->ApiVersion, Options->SessionName, Options->SessionName, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Sessions_DestroySession (%p, %p, %p)", Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_SESSIONS_DESTROYSESSION_API_LATEST);

	PROXY_FUNC(EOS_Sessions_DestroySession);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// TODOASYNC
	return;
}

/**
 * Join a session, creating a local session under a given session name.  Backend will validate various conditions to make sure it is possible to join the session.
 *
 * @param Options Structure containing information about the session to be joined
 * @param ClientData Arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate A callback that is fired when the join operation completes, either successfully or in error
 *
 * @return EOS_Success if the destroy completes successfully
 *         EOS_InvalidParameters if any of the options are incorrect
 *         EOS_Sessions_SessionAlreadyExists if the session is already exists or is in the process of being joined
 */
EOS_DECLARE_FUNC(void) EOS_Sessions_JoinSession(EOS_HSessions Handle, const EOS_Sessions_JoinSessionOptions* Options, void* ClientData, const EOS_Sessions_OnJoinSessionCallback CompletionDelegate)
{
	if (Options && Options->SessionName)
		log(LL::Debug, "EOS_Sessions_JoinSession (%p(%d, %p(%s), %p, %p), %p, %p)", Options, Options->ApiVersion, Options->SessionName, Options->SessionName, Options->SessionHandle, Options->LocalUserId, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Sessions_JoinSession (%p, %p, %p)", Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_SESSIONS_JOINSESSION_API_LATEST);

	PROXY_FUNC(EOS_Sessions_JoinSession);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// TODOASYNC
	return;
}

/**
 * Mark a session as started, making it unable to find if session properties indicate "join in progress" is not available
 *
 * @param Options Structure containing information about the session to be started
 * @param ClientData Arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate A callback that is fired when the start operation completes, either successfully or in error
 *
 * @return EOS_Success if the start completes successfully
 *         EOS_InvalidParameters if any of the options are incorrect
 *         EOS_Sessions_OutOfSync if the session is out of sync and will be updated on the next connection with the backend
 *         EOS_NotFound if a session to be started does not exist
 */
EOS_DECLARE_FUNC(void) EOS_Sessions_StartSession(EOS_HSessions Handle, const EOS_Sessions_StartSessionOptions* Options, void* ClientData, const EOS_Sessions_OnStartSessionCallback CompletionDelegate)
{
	if (Options && Options->SessionName)
		log(LL::Debug, "EOS_Sessions_StartSession (%p(%d, %p(%s)), %p, %p)", Options, Options->ApiVersion, Options->SessionName, Options->SessionName, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Sessions_StartSession (%p, %p, %p)", Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_SESSIONS_STARTSESSION_API_LATEST);

	PROXY_FUNC(EOS_Sessions_StartSession);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// TODOASYNC
	return;
}

/**
 * Mark a session as ended, making it available to find if "join in progress" was disabled.  The session may be started again if desired
 *
 * @param Options Structure containing information about the session to be ended
 * @param ClientData Arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate A callback that is fired when the end operation completes, either successfully or in error
 *
 * @return EOS_Success if the end completes successfully
 *         EOS_InvalidParameters if any of the options are incorrect
 *         EOS_Sessions_OutOfSync if the session is out of sync and will be updated on the next connection with the backend
 *         EOS_NotFound if a session to be ended does not exist
 */
EOS_DECLARE_FUNC(void) EOS_Sessions_EndSession(EOS_HSessions Handle, const EOS_Sessions_EndSessionOptions* Options, void* ClientData, const EOS_Sessions_OnEndSessionCallback CompletionDelegate)
{
	if (Options && Options->SessionName)
		log(LL::Debug, "EOS_Sessions_EndSession (%p(%d, %p(%s)), %p, %p)", Options, Options->ApiVersion, Options->SessionName, Options->SessionName, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Sessions_EndSession (%p, %p, %p)", Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_SESSIONS_ENDSESSION_API_LATEST);

	PROXY_FUNC(EOS_Sessions_EndSession);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// TODOASYNC
	return;
}

/**
 * Register a group of players with the session, allowing them to invite others or otherwise indicate they are part of the session for determining a full session
 *
 * @param Options Structure containing information about the session and players to be registered
 * @param ClientData Arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate A callback that is fired when the registration operation completes, either successfully or in error
 *
 * @return EOS_Success if the register completes successfully
 *         EOS_NoChange if the players to register registered previously
 *         EOS_InvalidParameters if any of the options are incorrect
 *         EOS_Sessions_OutOfSync if the session is out of sync and will be updated on the next connection with the backend
 *         EOS_NotFound if a session to register players does not exist
 */
EOS_DECLARE_FUNC(void) EOS_Sessions_RegisterPlayers(EOS_HSessions Handle, const EOS_Sessions_RegisterPlayersOptions* Options, void* ClientData, const EOS_Sessions_OnRegisterPlayersCallback CompletionDelegate)
{
	if (Options && Options->SessionName)
		log(LL::Debug, "EOS_Sessions_RegisterPlayers (%p(%d, %p(%s), %p, %d), %p, %p)", Options, Options->ApiVersion, Options->SessionName, Options->SessionName, Options->PlayersToRegister, Options->PlayersToRegisterCount, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Sessions_RegisterPlayers (%p, %p, %p)", Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_SESSIONS_REGISTERPLAYERS_API_LATEST);

	PROXY_FUNC(EOS_Sessions_RegisterPlayers);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// TODOASYNC
	return;
}

/**
 * Unregister a group of players with the session, freeing up space for others to join
 *
 * @param Options Structure containing information about the session and players to be unregistered
 * @param ClientData Arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate A callback that is fired when the unregistration operation completes, either successfully or in error
 *
 * @return EOS_Success if the unregister completes successfully
 *         EOS_NoChange if the players to unregister were not found
 *         EOS_InvalidParameters if any of the options are incorrect
 *         EOS_Sessions_OutOfSync if the session is out of sync and will be updated on the next connection with the backend
 *         EOS_NotFound if a session to be unregister players does not exist
 */
EOS_DECLARE_FUNC(void) EOS_Sessions_UnregisterPlayers(EOS_HSessions Handle, const EOS_Sessions_UnregisterPlayersOptions* Options, void* ClientData, const EOS_Sessions_OnUnregisterPlayersCallback CompletionDelegate)
{
	if (Options && Options->SessionName)
		log(LL::Debug, "EOS_Sessions_UnregisterPlayers (%p(%d, %p(%s), %p, %d), %p, %p)", Options, Options->ApiVersion, Options->SessionName, Options->SessionName, Options->PlayersToUnregister, Options->PlayersToUnregisterCount, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Sessions_UnregisterPlayers (%p, %p, %p)", Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_SESSIONS_UNREGISTERPLAYERS_API_LATEST);

	PROXY_FUNC(EOS_Sessions_UnregisterPlayers);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// TODOASYNC
	return;
}

/**
 * Send an invite to another player.  User must have created the session or be registered in the session or else the call will fail
 *
 * @param Options Structure containing information about the session and player to invite
 * @param ClientData Arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate A callback that is fired when the send invite operation completes, either successfully or in error
 *
 * @return EOS_Success if the send invite completes successfully
 *         EOS_InvalidParameters if any of the options are incorrect
 *         EOS_NotFound if a session to send invite does not exist
 */
EOS_DECLARE_FUNC(void) EOS_Sessions_SendInvite(EOS_HSessions Handle, const EOS_Sessions_SendInviteOptions* Options, void* ClientData, const EOS_Sessions_OnSendInviteCallback CompletionDelegate)
{
	if (Options && Options->SessionName)
		log(LL::Debug, "EOS_Sessions_SendInvite (%p(%d, %p(%s), %p, %p), %p, %p)", Options, Options->ApiVersion, Options->SessionName, Options->SessionName, Options->LocalUserId, Options->TargetUserId, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Sessions_SendInvite (%p, %p, %p)", Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_SESSIONS_SENDINVITE_API_LATEST);

	PROXY_FUNC(EOS_Sessions_SendInvite);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// TODOASYNC
	return;
}

/**
 * Create a session search handle.  This handle may be modified to include various search parameters.
 * Searching is possible in three methods, all mutually exclusive
 * - set the session id to find a specific session
 * - set the target user id to find a specific user
 * - set session parameters to find an array of sessions that match the search criteria
 *
 * @param Options Structure containing required parameters such as the maximum number of search results
 * @param OutSessionSearchHandle The new search handle or null if there was an error creating the search handle
 *
 * @return EOS_Success if the search creation completes successfully
 *         EOS_InvalidParameters if any of the options are incorrect
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Sessions_CreateSessionSearch(EOS_HSessions Handle, const EOS_Sessions_CreateSessionSearchOptions* Options, EOS_HSessionSearch* OutSessionSearchHandle)
{
	if (Options)
		log(LL::Debug, "EOS_Sessions_CreateSessionSearch (%p(%d, %d), %p)", Options, Options->ApiVersion, Options->MaxSearchResults, OutSessionSearchHandle);
	else
		log(LL::Debug, "EOS_Sessions_CreateSessionSearch (%p, %p)", Options, OutSessionSearchHandle);

	EOS_CHECK_VERSION(EOS_SESSIONS_CREATESESSIONSEARCH_API_LATEST);

	PROXY_FUNC(EOS_Sessions_CreateSessionSearch);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutSessionSearchHandle);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_InvalidParameters;
}

/**
 * Create a handle to an existing active session.
 *
 * @param Options Structure containing information about the active session to retrieve
 * @param OutSessionHandle The new active session handle or null if there was an error
 *
 * @return EOS_Success if the session handle was created successfully
 *         EOS_InvalidParameters if any of the options are incorrect
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 *         EOS_NotFound if the active session doesn't exist
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Sessions_CopyActiveSessionHandle(EOS_HSessions Handle, const EOS_Sessions_CopyActiveSessionHandleOptions* Options, EOS_HActiveSession* OutSessionHandle)
{
	if (Options && Options->SessionName)
		log(LL::Debug, "EOS_Sessions_CopyActiveSessionHandle (%p(%d, %p(%s)), %p)", Options, Options->ApiVersion, Options->SessionName, Options->SessionName, OutSessionHandle);
	else
		log(LL::Debug, "EOS_Sessions_CopyActiveSessionHandle (%p, %p)", Options, OutSessionHandle);

	EOS_CHECK_VERSION(EOS_SESSIONS_COPYACTIVESESSIONHANDLE_API_LATEST);

	PROXY_FUNC(EOS_Sessions_CopyActiveSessionHandle);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutSessionHandle);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_InvalidParameters;
}

/**
 * Register to receive session invites.
 * @note must call RemoveNotifySessionInviteReceived to remove the notification
 *
 * @param Options Structure containing information about the session invite notification
 * @param ClientData Arbitrary data that is passed back to you in the CompletionDelegate
 * @param Notification A callback that is fired when a session invite for a user has been received
 *
 * @return handle representing the registered callback
 */
EOS_DECLARE_FUNC(EOS_NotificationId) EOS_Sessions_AddNotifySessionInviteReceived(EOS_HSessions Handle, const EOS_Sessions_AddNotifySessionInviteReceivedOptions* Options, void* ClientData, const EOS_Sessions_OnSessionInviteReceivedCallback NotificationFn)
{
	if (Options)
		log(LL::Debug, "EOS_Sessions_AddNotifySessionInviteReceived (%p(%d), %p, %p)", Options, Options->ApiVersion, ClientData, NotificationFn);
	else
		log(LL::Debug, "EOS_Sessions_AddNotifySessionInviteReceived (%p, %p, %p)", Options, ClientData, NotificationFn);

	EOS_CHECK_VERSION(EOS_SESSIONS_ADDNOTIFYSESSIONINVITERECEIVED_API_LATEST);

	PROXY_FUNC(EOS_Sessions_AddNotifySessionInviteReceived);
	if (proxied)
	{
		auto res = proxied(Handle, Options, ClientData, NotificationFn);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	// TODOSTUB
	return EOS_INVALID_NOTIFICATIONID;
}

/**
 * Unregister from receiving session invites.
 *
 * @param InId Handle representing the registered callback
 */
EOS_DECLARE_FUNC(void) EOS_Sessions_RemoveNotifySessionInviteReceived(EOS_HSessions Handle, EOS_NotificationId InId)
{
	log(LL::Debug, "EOS_Sessions_RemoveNotifySessionInviteReceived (%d)", InId);

	PROXY_FUNC(EOS_Sessions_RemoveNotifySessionInviteReceived);
	if (proxied)
	{
		proxied(Handle, InId);
		return;
	}

	// TODOSTUB
	return;
}

/**
 * EOS_Sessions_CopySessionHandleByInviteId is used to immediately retrieve a handle to the session information from after notification of an invite
 * If the call returns an EOS_Success result, the out parameter, OutSessionHandle, must be passed to EOS_SessionDetails_Release to release the memory associated with it.
 *
 * @param Options Structure containing the input parameters
 * @param OutSessionHandle out parameter used to receive the session handle
 *
 * @return EOS_Success if the information is available and passed out in OutSessionHandle
 *         EOS_InvalidParameters if you pass an invalid invite id or a null pointer for the out parameter
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 *
 * @see EOS_Sessions_CopySessionHandleByInviteIdOptions
 * @see EOS_SessionDetails_Release
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Sessions_CopySessionHandleByInviteId(EOS_HSessions Handle, const EOS_Sessions_CopySessionHandleByInviteIdOptions* Options, EOS_HSessionDetails* OutSessionHandle)
{
	if (Options && Options->InviteId)
		log(LL::Debug, "EOS_Sessions_CopySessionHandleByInviteId (%p(%d, %p(%s)), %p)", Options, Options->ApiVersion, Options->InviteId, Options->InviteId, OutSessionHandle);
	else
		log(LL::Debug, "EOS_Sessions_CopySessionHandleByInviteId (%p, %p)", Options, OutSessionHandle);

	EOS_CHECK_VERSION(EOS_SESSIONS_COPYSESSIONHANDLEBYINVITEID_API_LATEST);

	PROXY_FUNC(EOS_Sessions_CopySessionHandleByInviteId);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutSessionHandle);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_InvalidParameters;
}

/**
 * Dump the contents of active sessions that exist locally to the log output, purely for debug purposes
 *
 * @param Options Options related to dumping session state such as the session name
 *
 * @return EOS_Success if the output operation completes successfully
 *         EOS_NotFound if the session specified does not exist
 *         EOS_InvalidParameters if any of the options are incorrect
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_Sessions_DumpSessionState(EOS_HSessions Handle, const EOS_Sessions_DumpSessionStateOptions* Options)
{
	if (Options && Options->SessionName)
		log(LL::Debug, "EOS_Sessions_DumpSessionState (%p(%d, %p(%s)))", Options, Options->ApiVersion, Options->SessionName, Options->SessionName);
	else
		log(LL::Debug, "EOS_Sessions_DumpSessionState (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONS_DUMPSESSIONSTATE_API_LATEST);

	PROXY_FUNC(EOS_Sessions_DumpSessionState);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * To modify sessions, you must call EOS_Sessions_CreateSessionModification to create a Session Modification handle. To modify that handle, call
 * EOS_SessionModification_* methods. Once you are finished, call EOS_Sessions_UpdateSession with your handle. You must then release your Session Modification
 * handle by calling EOS_SessionModification_Release.
 */

/**
 * Set the bucket id associated with this session.
 * Values such as region, game mode, etc can be combined here depending on game need.
 * Setting this is strongly recommended to improve search performance.
 *
 * @param Options Options associated with the bucket id of the session
 *
 * @return EOS_Success if setting this parameter was successful
 *         EOS_InvalidParameters if the bucket id is invalid or null
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionModification_SetBucketId(EOS_HSessionModification Handle, const EOS_SessionModification_SetBucketIdOptions* Options)
{
	if (Options && Options->BucketId)
		log(LL::Debug, "EOS_SessionModification_SetBucketId (%p(%d, %p(%s)))", Options, Options->ApiVersion, Options->BucketId, Options->BucketId);
	else
		log(LL::Debug, "EOS_SessionModification_SetBucketId (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONMODIFICATION_SETBUCKETID_API_LATEST);

	PROXY_FUNC(EOS_SessionModification_SetBucketId);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * Set the host address associated with this session
 * Setting this is optional, if the value is not set the SDK will fill the value in from the service.
 * It is useful to set if other addressing mechanisms are desired or if LAN addresses are preferred during development
 *
 * NOTE: No validation of this value occurs to allow for flexibility in addressing methods
 *
 * @param Options Options associated with the host address of the session
 *
 * @return EOS_Success if setting this parameter was successful
 *         EOS_InvalidParameters if the host id is an empty string
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionModification_SetHostAddress(EOS_HSessionModification Handle, const EOS_SessionModification_SetHostAddressOptions* Options)
{
	if (Options && Options->HostAddress)
		log(LL::Debug, "EOS_SessionModification_SetHostAddress (%p(%d, %p(%s)))", Options, Options->ApiVersion, Options->HostAddress, Options->HostAddress);
	else
		log(LL::Debug, "EOS_SessionModification_SetHostAddress (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONMODIFICATION_SETHOSTADDRESS_API_LATEST);

	PROXY_FUNC(EOS_SessionModification_SetHostAddress);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * Set the session permissions associated with this session.
 * The permissions range from "public" to "invite only" and are described by EOS_EOnlineSessionPermissionLevel
 *
 * @param Options Options associated with the permission level of the session
 *
 * @return EOS_Success if setting this parameter was successful
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionModification_SetPermissionLevel(EOS_HSessionModification Handle, const EOS_SessionModification_SetPermissionLevelOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_SessionModification_SetPermissionLevel (%p(%d, %d))", Options, Options->ApiVersion, Options->PermissionLevel);
	else
		log(LL::Debug, "EOS_SessionModification_SetPermissionLevel (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONMODIFICATION_SETPERMISSIONLEVEL_API_LATEST);

	PROXY_FUNC(EOS_SessionModification_SetPermissionLevel);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * Set whether or not join in progress is allowed
 * Once a session is started, it will no longer be visible to search queries unless this flag is set or the session returns to the pending or ended state
 *
 * @param Options Options associated with setting the join in progress state the session
 *
 * @return EOS_Success if setting this parameter was successful
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionModification_SetJoinInProgressAllowed(EOS_HSessionModification Handle, const EOS_SessionModification_SetJoinInProgressAllowedOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_SessionModification_SetJoinInProgressAllowed (%p(%d, %d))", Options, Options->ApiVersion, Options->bAllowJoinInProgress);
	else
		log(LL::Debug, "EOS_SessionModification_SetJoinInProgressAllowed (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONMODIFICATION_SETJOININPROGRESSALLOWED_API_LATEST);

	PROXY_FUNC(EOS_SessionModification_SetJoinInProgressAllowed);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * Set the maximum number of players allowed in this session.
 * When updating the session, it is not possible to reduce this number below the current number of existing players
 *
 * @param Options Options associated with max number of players in this session
 *
 * @return EOS_Success if setting this parameter was successful
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionModification_SetMaxPlayers(EOS_HSessionModification Handle, const EOS_SessionModification_SetMaxPlayersOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_SessionModification_SetMaxPlayers (%p(%d, %d))", Options, Options->ApiVersion, Options->MaxPlayers);
	else
		log(LL::Debug, "EOS_SessionModification_SetMaxPlayers (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONMODIFICATION_SETMAXPLAYERS_API_LATEST);

	PROXY_FUNC(EOS_SessionModification_SetMaxPlayers);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * Associate an attribute with this session
 * An attribute is something that may or may not be advertised with the session.
 * If advertised, it can be queried for in a search, otherwise the data remains local to the client
 *
 * @param Options Options to set the attribute and its advertised state
 *
 * @return EOS_Success if setting this parameter was successful
 *		   EOS_InvalidParameters if the attribution is missing information or otherwise invalid
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionModification_AddAttribute(EOS_HSessionModification Handle, const EOS_SessionModification_AddAttributeOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_SessionModification_AddAttribute (%p(%d, %p, %d))", Options, Options->ApiVersion, Options->SessionAttribute, Options->AdvertisementType);
	else
		log(LL::Debug, "EOS_SessionModification_AddAttribute (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONMODIFICATION_ADDATTRIBUTE_API_LATEST);

	PROXY_FUNC(EOS_SessionModification_AddAttribute);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * Remove an attribute from this session
 *
 * @param Options Specify the key of the attribute to remove
 *
 * @return EOS_Success if removing this parameter was successful
 *		   EOS_InvalidParameters if the key is null or empty
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionModification_RemoveAttribute(EOS_HSessionModification Handle, const EOS_SessionModification_RemoveAttributeOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_SessionModification_RemoveAttribute (%p(%d, %p(%s)))", Options, Options->ApiVersion, Options->Key, Options->Key);
	else
		log(LL::Debug, "EOS_SessionModification_RemoveAttribute (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONMODIFICATION_REMOVEATTRIBUTE_API_LATEST);

	PROXY_FUNC(EOS_SessionModification_RemoveAttribute);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * This class is not exposed yet, but is the manifestation of a session some local player is actively involved in (via Create/Join)
 */

/**
 * EOS_ActiveSession_CopyInfo is used to immediately retrieve a copy of active session information
 * If the call returns an EOS_Success result, the out parameter, OutActiveSessionInfo, must be passed to EOS_ActiveSession_Info_Release to release the memory associated with it.
 *
 * @param Options Structure containing the input parameters
 * @param OutActiveSessionInfo Out parameter used to receive the EOS_ActiveSession_Info structure.
 *
 * @return EOS_Success if the information is available and passed out in OutActiveSessionInfo
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 *
 * @see EOS_ActiveSession_Info
 * @see EOS_ActiveSession_CopyInfoOptions
 * @see EOS_ActiveSession_Info_Release
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_ActiveSession_CopyInfo(EOS_HActiveSession Handle, const EOS_ActiveSession_CopyInfoOptions* Options, EOS_ActiveSession_Info ** OutActiveSessionInfo)
{
	if (Options)
		log(LL::Debug, "EOS_ActiveSession_CopyInfo (%p(%d, %d), %p)", Options, Options->ApiVersion, OutActiveSessionInfo);
	else
		log(LL::Debug, "EOS_ActiveSession_CopyInfo (%p, %p)", Options, OutActiveSessionInfo);

	EOS_CHECK_VERSION(EOS_ACTIVESESSION_COPYINFO_API_LATEST);

	PROXY_FUNC(EOS_ActiveSession_CopyInfo);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutActiveSessionInfo);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_InvalidParameters;
}

/**
 * This class represents the details of a session, including its session properties and the attribution associated with it
 * Locally created or joined active sessions will contain this information as will search results.   
 * A handle to a session is required to join a session via search or invite
 */

/**
 * EOS_SessionDetails_CopyInfo is used to immediately retrieve a copy of session information from a given source such as a active session or a search result.
 * If the call returns an EOS_Success result, the out parameter, OutSessionInfo, must be passed to EOS_SessionDetails_Info_Release to release the memory associated with it.
 *
 * @param Options Structure containing the input parameters
 * @param OutSessionInfo Out parameter used to receive the EOS_SessionDetails_Info structure.
 *
 * @return EOS_Success if the information is available and passed out in OutSessionInfo
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 *
 * @see EOS_SessionDetails_Info
 * @see EOS_SessionDetails_CopyInfoOptions
 * @see EOS_SessionDetails_Info_Release
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionDetails_CopyInfo(EOS_HSessionDetails Handle, const EOS_SessionDetails_CopyInfoOptions* Options, EOS_SessionDetails_Info ** OutSessionInfo)
{
	if (Options)
		log(LL::Debug, "EOS_SessionDetails_CopyInfo (%p(%d, %d), %p)", Options, Options->ApiVersion, OutSessionInfo);
	else
		log(LL::Debug, "EOS_SessionDetails_CopyInfo (%p, %p)", Options, OutSessionInfo);

	EOS_CHECK_VERSION(EOS_SESSIONDETAILS_COPYINFO_API_LATEST);

	PROXY_FUNC(EOS_SessionDetails_CopyInfo);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutSessionInfo);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_InvalidParameters;
}

/**
 * Get the number of attributes associated with this session
 *
 * @param Options the Options associated with retrieving the attribute count
 *
 * @return number of attributes on the session or 0 if there is an error
 */
EOS_DECLARE_FUNC(uint32_t) EOS_SessionDetails_GetSessionAttributeCount(EOS_HSessionDetails Handle, const EOS_SessionDetails_GetSessionAttributeCountOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_SessionDetails_GetSessionAttributeCount (%p(%d))", Options, Options->ApiVersion);
	else
		log(LL::Debug, "EOS_SessionDetails_GetSessionAttributeCount (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONDETAILS_GETSESSIONATTRIBUTECOUNT_API_LATEST);

	PROXY_FUNC(EOS_SessionDetails_GetSessionAttributeCount);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	// TODOSTUB
	return 0;
}

/**
 * EOS_SessionDetails_CopySessionAttributeByIndex is used to immediately retrieve a copy of session attribution from a given source such as a active session or a search result.
 * If the call returns an EOS_Success result, the out parameter, OutSessionAttribute, must be passed to EOS_SessionDetails_Attribute_Release to release the memory associated with it.
 *
 * @param Options Structure containing the input parameters
 * @param OutSessionAttribute Out parameter used to receive the EOS_SessionDetails_Attribute structure.
 *
 * @return EOS_Success if the information is available and passed out in OutSessionAttribute
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 *
 * @see EOS_SessionDetails_Info
 * @see EOS_SessionDetails_CopySessionAttributeByIndexOptions
 * @see EOS_SessionDetails_Attribute_Release
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionDetails_CopySessionAttributeByIndex(EOS_HSessionDetails Handle, const EOS_SessionDetails_CopySessionAttributeByIndexOptions* Options, EOS_SessionDetails_Attribute ** OutSessionAttribute)
{
	if (Options)
		log(LL::Debug, "EOS_SessionDetails_CopySessionAttributeByIndex (%p(%d, %d))", Options, Options->ApiVersion, Options->AttrIndex);
	else
		log(LL::Debug, "EOS_SessionDetails_CopySessionAttributeByIndex (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONDETAILS_COPYSESSIONATTRIBUTEBYINDEX_API_LATEST);

	PROXY_FUNC(EOS_SessionDetails_CopySessionAttributeByIndex);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutSessionAttribute);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_InvalidParameters;
}

/**
 * Class responsible for the creation, setup, and execution of a search query.
 * Search parameters are defined, the query is executed and the search results are returned within this object
 */

/**
 * Set a session id to find and will return at most one search result.  Setting TargetUserId or SearchParameters will result in EOS_SessionSearch_Find failing
 *
 * @param Options A specific session id for which to search
 *
 * @return EOS_Success if setting this session id was successful
 *         EOS_InvalidParameters if the session id is invalid or null
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionSearch_SetSessionId(EOS_HSessionSearch Handle, const EOS_SessionSearch_SetSessionIdOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_SessionSearch_SetSessionId (%p(%d, %p(%s)))", Options, Options->ApiVersion, Options->SessionId);
	else
		log(LL::Debug, "EOS_SessionSearch_SetSessionId (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONSEARCH_SETSESSIONID_API_LATEST);

	PROXY_FUNC(EOS_SessionSearch_SetSessionId);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * Set a target user id to find and will return at most one search result.  Setting SessionId or SearchParameters will result in EOS_SessionSearch_Find failing
 * NOTE: a search result will only be found if this user is in a public session
 *
 * @param Options a specific target user id to find
 *
 * @return EOS_Success if setting this session id was successful
 *         EOS_InvalidParameters if the target user id is invalid or null
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionSearch_SetTargetUserId(EOS_HSessionSearch Handle, const EOS_SessionSearch_SetTargetUserIdOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_SessionSearch_SetTargetUserId (%p(%d, %p))", Options, Options->ApiVersion, Options->TargetUserId);
	else
		log(LL::Debug, "EOS_SessionSearch_SetTargetUserId (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONSEARCH_SETTARGETUSERID_API_LATEST);

	PROXY_FUNC(EOS_SessionSearch_SetTargetUserId);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * Add a parameter to an array of search criteria combined via an AND operator.  Setting SessionId or TargetUserId will result in EOS_SessionSearch_Find failing
 *
 * @param Options a search parameter and its comparison op
 *
 * @return EOS_Success if setting this search parameter was successful
 *         EOS_InvalidParameters if the search criteria is invalid or null
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 *
 * @see EOS_Sessions_AttributeData
 * @see EOS_EOnlineComparisonOp
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionSearch_SetParameter(EOS_HSessionSearch Handle, const EOS_SessionSearch_SetParameterOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_SessionSearch_SetParameter (%p(%d, %p, %d))", Options, Options->ApiVersion, Options->Parameter, Options->ComparisonOp);
	else
		log(LL::Debug, "EOS_SessionSearch_SetParameter (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONSEARCH_SETPARAMETER_API_LATEST);

	PROXY_FUNC(EOS_SessionSearch_SetParameter);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * Remove a parameter from the array of search criteria.
 *
 * @params Options a search parameter key name to remove
 *
 * @return EOS_Success if removing this search parameter was successful
 *         EOS_InvalidParameters if the search key is invalid or null
 *		   EOS_NotFound if the parameter was not a part of the search criteria
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionSearch_RemoveParameter(EOS_HSessionSearch Handle, const EOS_SessionSearch_RemoveParameterOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_SessionSearch_RemoveParameter (%p(%d, %p(%s), %d))", Options, Options->ApiVersion, Options->Key, Options->ComparisonOp);
	else
		log(LL::Debug, "EOS_SessionSearch_RemoveParameter (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONSEARCH_REMOVEPARAMETER_API_LATEST);

	PROXY_FUNC(EOS_SessionSearch_RemoveParameter);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * Set the maximum number of search results to return in the query, can't be more than EOS_SESSIONS_MAX_SEARCH_RESULTS
 *
 * @param Options maximum number of search results to return in the query
 *
 * @return EOS_Success if setting the max results was successful
 *         EOS_InvalidParameters if the number of results requested is invalid
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionSearch_SetMaxResults(EOS_HSessionSearch Handle, const EOS_SessionSearch_SetMaxResultsOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_SessionSearch_SetMaxResults (%p(%d, %d))", Options, Options->ApiVersion, Options->MaxSearchResults);
	else
		log(LL::Debug, "EOS_SessionSearch_SetMaxResults (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONSEARCH_SETMAXSEARCHRESULTS_API_LATEST);

	PROXY_FUNC(EOS_SessionSearch_SetMaxResults);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	// TODOSTUB
	return EOS_EResult::EOS_Success;
}

/**
 * Find sessions matching the search criteria setup via this session search handle.
 * When the operation completes, this handle will have the search results that can be parsed
 *
 * @param Options Structure containing information about the search criteria to use
 * @param ClientData Arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate A callback that is fired when the search operation completes, either successfully or in error
 *
 * @return EOS_Success if the find operation completes successfully
 *         EOS_NotFound if searching for an individual session by sessionid or targetuserid returns no results
 *         EOS_InvalidParameters if any of the options are incorrect
 */
EOS_DECLARE_FUNC(void) EOS_SessionSearch_Find(EOS_HSessionSearch Handle, const EOS_SessionSearch_FindOptions* Options, void* ClientData, const EOS_SessionSearch_OnFindCallback CompletionDelegate)
{
	if (Options)
		log(LL::Debug, "EOS_SessionSearch_Find (%p(%d))", Options, Options->ApiVersion);
	else
		log(LL::Debug, "EOS_SessionSearch_Find (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONSEARCH_FIND_API_LATEST);

	PROXY_FUNC(EOS_SessionSearch_Find);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// TODOASYNC
	return;
}

/**
 * Get the number of search results found by the search parameters in this search
 *
 * @param Options Options associated with the search count
 *
 * @return return the number of search results found by the query or 0 if search is not complete
 */
EOS_DECLARE_FUNC(uint32_t) EOS_SessionSearch_GetSearchResultCount(EOS_HSessionSearch Handle, const EOS_SessionSearch_GetSearchResultCountOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_SessionSearch_GetSearchResultCount (%p(%d))", Options, Options->ApiVersion);
	else
		log(LL::Debug, "EOS_SessionSearch_GetSearchResultCount (%p)", Options);

	EOS_CHECK_VERSION(EOS_SESSIONSEARCH_GETSEARCHRESULTCOUNT_API_LATEST);

	PROXY_FUNC(EOS_SessionSearch_GetSearchResultCount);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	// TODOSTUB
	return 0;
}

/**
 * EOS_SessionSearch_CopySearchResultByIndex is used to immediately retrieve a handle to the session information from a given search result.
 * If the call returns an EOS_Success result, the out parameter, OutSessionHandle, must be passed to EOS_SessionDetails_Release to release the memory associated with it.
 *
 * @param Options Structure containing the input parameters
 * @param OutSessionHandle out parameter used to receive the session handle
 *
 * @return EOS_Success if the information is available and passed out in OutSessionHandle
 *         EOS_InvalidParameters if you pass an invalid index or a null pointer for the out parameter
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 *
 * @see EOS_SessionSearch_CopySearchResultByIndexOptions
 * @see EOS_SessionDetails_Release
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_SessionSearch_CopySearchResultByIndex(EOS_HSessionSearch Handle, const EOS_SessionSearch_CopySearchResultByIndexOptions* Options, EOS_HSessionDetails* OutSessionHandle)
{
	if(Options)
		log(LL::Debug, "EOS_SessionSearch_CopySearchResultByIndex (%p(%d, %d), %p)", Options, Options->ApiVersion, Options->SessionIndex, OutSessionHandle);
	else
		log(LL::Debug, "EOS_SessionSearch_CopySearchResultByIndex (%p, %p)", Options, OutSessionHandle);

	EOS_CHECK_VERSION(EOS_SESSIONSEARCH_COPYSEARCHRESULTBYINDEX_API_LATEST);

	PROXY_FUNC(EOS_SessionSearch_CopySearchResultByIndex);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutSessionHandle);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	return EOS_EResult::EOS_InvalidParameters; // TODOSTUB
}
