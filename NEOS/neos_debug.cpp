#include "stdafx.h"
#include "NEOS.hpp"

// INI configurable settings
bool LogEnabled = true;
std::string LogFilename = NEOS_LOG_FILE;

#ifdef _DEBUG
int LogLevel = (int)LL::Debug;
#else
int LogLevel = (int)LL::Info;
#endif

// Names to use when writing to log...
const char* LogLevelNames[] =
{
	"DBG",
	"INF",
	"WRN",
	"ERR"
};

std::mutex LoggingMutex;

void log(LL Severity, const char* Format, ...)
{
	if (!LogEnabled)
		return;

	int sev_int = (int)Severity;

	if (LogLevel > sev_int)
		return;

	// Set severity to error if it's invalid
	if (sev_int >= (uint32_t)LL::NumLevels)
		sev_int = (uint32_t)LL::Error;

	char* str = new char[4096];
	va_list ap;
	va_start(ap, Format);

	vsnprintf(str, 4096, Format, ap);
	va_end(ap);

	{
		std::lock_guard<std::mutex> guard(LoggingMutex);

		std::ofstream file;
		file.open(NEOSFolder / LogFilename, std::ofstream::out | std::ofstream::app);
		if (!file.is_open())
			return; // wtf

		file << "[" << LogLevelNames[sev_int] << "] ";
		file << str << "\n";

		file.close();
	}
}

// Callback for EOS sdk log messages
void EOS_CALL EOSSDKLoggingCallback(const EOS_LogMessage* InMsg)
{
	log(LL::Debug, "+ EOSSDK: %s: %s", InMsg->Category, InMsg->Message);
}

bool NEOS_IsProxyLogged = false;

void NEOS_InitProxyLogging()
{
	// set our callback as the logging function

	auto result = EOS_EResult::EOS_Success;
	{
		PROXY_FUNC(EOS_Logging_SetCallback);
		if (!proxied)
			return; // failed to get SetCallback ptr

		result = proxied(&EOSSDKLoggingCallback);
	}

	NEOS_IsProxyLogged = result == EOS_EResult::EOS_Success;;

	// set log level for all components
	if(NEOS_IsProxyLogged)
	{
		PROXY_FUNC(EOS_Logging_SetLogLevel);
		if (!proxied)
			return;

		proxied(EOS_ELogCategory::EOS_LC_ALL_CATEGORIES, EOS_ELogLevel::EOS_LOG_Info);
	}
}
