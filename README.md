nEOS
====

Here is the source code for nEOS, an emulator/proxy DLL for Epic Online Services.

This is meant for educational purposes to help give an understanding of how the EOS system works. No game keys/tokens/IDs are provided with it!

More info can be found in the readme.txt & nEOS.ini files.

Any contributions or bug reports would be appreciated!


License
----

This code is licensed under the Apache License 2.0.

Makes use of fifo_map by nlohmann, licensed under MIT (https://github.com/nlohmann/fifo_map)

Makes use of inih by benhoyt, licensed under BSD (https://github.com/benhoyt/inih)
